package ggscms

import org.apache.shiro.SecurityUtils

class NewTestsTagLib {

    static namespace = "tests"

    Access membershipAccess(Long id) {
        String userName = SecurityUtils.subject?.principal
        User user = User.findByUsername("$userName")
        Course course
        if (id != null) {
            course = Course.findById(id)
        }
        else {
            course = Course.findById(params.id)
        }
        Membership membership = Membership.findByUserAndCourse(user, course)
        if (membership) {
            return membership.access
        }
        else return null
    }

    Integer countNewTests(id) {
        String userName = SecurityUtils.subject?.principal
        User user = User.findByUsername("$userName")
        Course courseInstance
        if (id != null) {
            courseInstance = Course.findById(id)
        }
        else {
            courseInstance = Course.findById(params.id)
        }

        int count = 0

        Test.where {
            course == courseInstance
            isVisible == true || ( visibleOnTime == true && date < new Date())
        }.list().each {
                if (!TestResult.findByUserAndTest(user,it)) {
                    count++
                }
        }

        if (count == 0) {
            return null
        } else return count
    }

    String checkTestComplete(id) {
        String userName = SecurityUtils.subject?.principal
        User user = User.findByUsername("$userName")
        Test test = Test.findById(id)

        if (TestResult.findByUserAndTest(user,test)) {
            return 'success'
        } else {
            return 'danger'
        }

    }

    def count = { attrs, body ->
        Long id = attrs["courseId"]
        if (SecurityUtils.subject.principal != null && membershipAccess(id) == Access.STUDENT) {
            out << countNewTests(id)
        }
    }

    def complete = { attrs, body ->
        Long testId = attrs["testId"]
        Long courseId = attrs["courseId"]
        if (SecurityUtils.subject.principal != null && membershipAccess(courseId) == Access.STUDENT) {
            out << checkTestComplete(testId)
        } else {
            out << 'default'
        }
    }

}
