package ggscms

import org.apache.shiro.SecurityUtils

class MembershipTagLib {

    static namespace = "membership"

    Access membershipAccess(Long id) {
        String userName = SecurityUtils.subject?.principal
        User user = User.findByUsername("$userName")
        Course course
        if (id != null) {
            course = Course.findById(id)
        }
        else {
            course = Course.findById(params.id)
        }
        Membership membership = Membership.findByUserAndCourse(user, course)
        if (membership) {
            return membership.access
        }
        else return null
    }

    def admin() {
        String userName = SecurityUtils.subject?.principal
        User user = User.findByUsername("$userName")
        Role adminRole = Role.findByName("administrator")
        if (user.roles.contains(adminRole)) {
            return true
        }
        return false
    }

    def teacher = { attrs, body ->
        Long id = attrs["courseId"]
        if (SecurityUtils.subject.principal != null && membershipAccess(id) == Access.TEACHER) {
            out << body()
        }
    }

    def student = { attrs, body ->
        Long id = attrs["courseId"]
        if (SecurityUtils.subject.principal != null && membershipAccess(id) == Access.STUDENT) {
            out << body()
        }
    }

    def listener = { attrs, body ->
        Long id = attrs["courseId"]
        if (SecurityUtils.subject.principal != null && membershipAccess(id) == Access.LISTENER) {
            out << body()
        }
    }

    def subscribed = { attrs, body ->
        Long id = attrs["courseId"]
        if (SecurityUtils.subject.principal != null && membershipAccess(id) == Access.SUBSCRIBED) {
            out << body()
        }
    }

    def none = { attrs, body ->
        Long id = attrs["courseId"]
        if (SecurityUtils.subject.principal != null && membershipAccess(id) == null) {
            out << body()
        }
    }

    def teacherOrAdmin = { attrs, body ->
        Long id = attrs["courseId"]
        if (SecurityUtils.subject.principal != null && ( membershipAccess(id) == Access.TEACHER || admin() )) {
            out << body()
        }
    }
}
