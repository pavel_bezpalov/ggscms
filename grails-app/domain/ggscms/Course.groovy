package ggscms

class Course {
    String name
    String shortDescription
    String description

    String toString() {
        "$name"
    }

    static hasMany = [memberships: Membership, chapters: Chapter, tests: Test, tasks: Task]

    static constraints = {
        name(blank: false, unique: true, size: 3..100)
        shortDescription(maxSize: 400, blank: false)
        description(maxSize: 10000, blank: false)
    }
}
