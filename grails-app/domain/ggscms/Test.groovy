package ggscms

import org.grails.databinding.BindingFormat

class Test {
    String name
    String description
    Boolean isVisible = false
    Integer timeLimit
    Boolean visibleOnTime = false
    Date date = null
    Integer number = null
    Boolean allowViewResults = false

    static belongsTo = [course: Course]
    static hasMany = [questions: Question]

    static constraints = {
        name(blank: false, size: 3..100)
        description(maxSize: 10000, blank: false)
        number(nullable: true)
        date(nullable: true)
        isVisible()
        timeLimit()
    }
}
