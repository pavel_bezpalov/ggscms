package ggscms

enum Access {

    TEACHER("Teacher"),
    STUDENT("Student"),
    LISTENER("Listener"),
    SUBSCRIBED("Subscribed")

    String value

    Access(String value) {
        this.value = value
    }

    @Override

    String toString() {
        value
    }
}

class Membership {
    User user
    Course course
    Access access

    static constraints = {
        course()
        user()
        access()
    }
}
