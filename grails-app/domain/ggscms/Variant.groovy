package ggscms

class Variant {
    String name
    Boolean correct = false

    static belongsTo = [question: Question]

    static constraints = {
        name(blank: false, size: 3..100)
    }
}
