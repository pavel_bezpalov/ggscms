package ggscms

class User {
    Date dateCreated
    String username
    String passwordHash
    String fullName
    String email
    String phone
    String membership

    static transients = ['membership']

    String toString() {
        "$username"
    }

	static mapping = { 
		table 'users'
	 }

    static hasMany = [memberships: Membership, roles: Role, permissions: String]

    static constraints = {
        username(nullable: false, blank: false, unique: true)
        fullName blank: false
        email email: true, blank: false
        phone blank: false
    }
}
