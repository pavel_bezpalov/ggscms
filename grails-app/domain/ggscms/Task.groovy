package ggscms

import org.grails.databinding.BindingFormat

enum Type {

    LESSON("Lesson"),
    HOMEWORK("Homework")

    String value

    Type(String value) {
        this.value = value
    }

    @Override

    String toString() {
        value
    }
}

class Task {
    String name
    Type type
    String description
    Date date = null
    Boolean isVisible = false
    Boolean visibleOnTime = false
    Integer number = null
    Color color = Color.DEFAULT

    String toString() {
        "$name"
    }

    static belongsTo = [course: Course]

    static constraints = {
        name(blank: false, size: 3..100)
        description(maxSize:10000, blank: false)
        number(nullable: true)
        date(nullable: true)
        isVisible()
        type()
    }
}
