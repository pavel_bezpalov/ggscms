package ggscms

class TestResult {
    User user
    Test test
    Date dateCreated
    Integer result

    static constraints = {
        dateCreated()
        user()
        test()
        result(nullable: true)
    }
}
