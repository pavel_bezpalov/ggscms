package ggscms

enum Color {

    DEFAULT("default"),
    PRIMARY("primary"),
    SUCCESS("success"),
    INFO("info"),
    WARNING("warning"),
    DANGER("danger")

    String value

    Color(String value) {
        this.value = value
    }

    @Override

    String toString() {
        value
    }
}

class Chapter {
    String name
    String description
    Date date
    Integer number = null
    Color color = Color.DEFAULT

    String toString() {
        "$name"
    }

    static belongsTo = [course: Course]

    static constraints = {
        name(blank: false, size: 3..100)
        description(maxSize:10000, blank: false)
        number(nullable: true)
        date(min: new Date())
    }

    static mapping = {
        sort date:"asc"
    }
}
