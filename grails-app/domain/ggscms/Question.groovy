package ggscms

class Question {
    String name
    String description

    static belongsTo = [test: Test]
    static hasMany = [variants: Variant]

    static constraints = {
        name(blank: false, size: 3..100)
        description(maxSize: 200, nullable: true)
    }
}
