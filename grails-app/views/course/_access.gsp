<membership:subscribed courseId="${course.id}">
    <p><g:remoteLink class="btn btn-info" action="subscribe" id="${course.id}"
                     update="${course.id}">Unsubscribe now!</g:remoteLink></p>
</membership:subscribed>
<membership:none courseId="${course.id}">
    <p><g:remoteLink class="btn btn-primary" action="subscribe" id="${course.id}"
                     update="${course.id}">Subscribe now!</g:remoteLink></p>
</membership:none>