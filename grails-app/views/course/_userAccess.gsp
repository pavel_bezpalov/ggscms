<%@ page import="ggscms.Access; ggscms.Membership" %>
${user.membership}
<g:if test="${Membership.findByUserAndCourse(user,course).access == Access.SUBSCRIBED}">
    <g:remoteLink class="btn btn-success" action="userAccessChange" id="${course.id}"
                  update="${user.id}" params="[userId: user.id, changeAccess: 'up']">Up</g:remoteLink>
</g:if>
<g:elseif test="${Membership.findByUserAndCourse(user,course).access == Access.TEACHER}">
    <shiro:hasAnyRole in="['administrator']">
    <g:remoteLink class="btn btn-danger" action="userAccessChange" id="${course.id}"
                  update="${user.id}" params="[userId: user.id, changeAccess: 'down']">Down</g:remoteLink>
    </shiro:hasAnyRole>
</g:elseif>
<g:elseif test="${Membership.findByUserAndCourse(user,course).access == Access.STUDENT}">
    <shiro:hasAnyRole in="['administrator']">
        <g:remoteLink class="btn btn-success" action="userAccessChange" id="${course.id}"
                      update="${user.id}" params="[userId: user.id, changeAccess: 'up']">Up</g:remoteLink>
    </shiro:hasAnyRole>
    <g:remoteLink class="btn btn-danger" action="userAccessChange" id="${course.id}"
                  update="${user.id}" params="[userId: user.id, changeAccess: 'down']">Down</g:remoteLink>
</g:elseif>
<g:else>
    <g:remoteLink class="btn btn-success" action="userAccessChange" id="${course.id}"
                  update="${user.id}" params="[userId: user.id, changeAccess: 'up']">Up</g:remoteLink>
    <g:remoteLink class="btn btn-danger" action="userAccessChange" id="${course.id}"
                  update="${user.id}" params="[userId: user.id, changeAccess: 'down']">Down</g:remoteLink>
</g:else>
