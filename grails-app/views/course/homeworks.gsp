<%@ page import="ggscms.Color; org.springframework.validation.FieldError; ggscms.Type; ggscms.Chapter; ggscms.Course" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'course.label', default: 'Course')}"/>
    <title><g:fieldValue bean="${courseInstance}"
                         field="name"/></title>
</head>

<body>
<content tag="navbar">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><g:fieldValue bean="${courseInstance}" field="name"/></a>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><g:link action="chapters" id="${courseInstance.id}">Course program</g:link></li>
                    <li><g:link action="lessons" id="${courseInstance.id}">Lessons</g:link></li>
                    <li class="active"><g:link action="homeworks" id="${courseInstance.id}">Homeworks</g:link></li>
                    <li><g:link action="tests" id="${courseInstance.id}">Tests <span class="badge"><tests:count/></span></g:link></li>
                    <li><g:link action="rating" id="${courseInstance.id}">Rating</g:link></li>
                    <membership:teacherOrAdmin>
                        <li><g:link action="users" id="${courseInstance.id}">Users</g:link></li>
                    </membership:teacherOrAdmin>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <g:render template="/common/topbar"/>
                </ul>
            </div><!--/.navbar-collapse -->
        </div>
    </nav>
</content>

<g:if test="${flash.message}">
    <div class="alert alert-info" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        ${flash.message}</div>
</g:if>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <g:each in="${homeworks}" status="i" var="taskInstance">
        <div class="panel panel-${taskInstance.color.value}">
            <div class="panel-heading" role="tab" id="collapseListGroupHeading${i}">
                <h4 id="-collapsible-list-group-" class="panel-title">
                    <a class="" data-toggle="collapse" href="#collapseListGroup${i}" aria-expanded="true"
                       aria-controls="collapseListGroup${i}">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-8 col-sm-8">
                                    <strong>${fieldValue(bean: taskInstance, field: "number")}</strong>
                                    ${fieldValue(bean: taskInstance, field: "name")}
                                </div>

                                <div class="col-xs-4 col-sm-4 text-right">
                                    <membership:teacherOrAdmin>
                                        <g:if test="${taskInstance.isVisible}">
                                            Always VISIBLE
                                        </g:if>
                                        <g:elseif
                                                test="${!taskInstance.isVisible && taskInstance.visibleOnTime && taskInstance.date < new Date()}">
                                            Already VISIBLE since <g:formatDate format="HH:mm, dd MMMM yyyy"
                                                                        date="${taskInstance.date}"/>
                                        </g:elseif>
                                        <g:elseif
                                                test="${!taskInstance.isVisible && taskInstance.visibleOnTime && taskInstance.date > new Date()}">
                                            INVISIBLE till <g:formatDate format="HH:mm, dd MMMM yyyy"
                                                                        date="${taskInstance.date}"/>
                                        </g:elseif>
                                        <g:else>
                                            Always INVISIBLE
                                        </g:else>
                                    </membership:teacherOrAdmin>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a class="anchorjs-link" href="#-collapsible-list-group-"><span class="anchorjs-icon"></span></a>
                </h4>
            </div>

            <div style="" aria-expanded="true" id="collapseListGroup${i}" class="panel-collapse collapse"
                 role="tabpanel" aria-labelledby="collapseListGroupHeading${i}">
                <ul class="list-group">
                    <li class="list-group-item">${raw(taskInstance.description)}</li>
                </ul>
                <membership:teacherOrAdmin>
                    <div class="panel-footer">
                        <g:link class="btn btn-warning" action="editTask"
                                id="${courseInstance.id}"
                                params="[taskId: taskInstance.id]"><g:message
                                code="default.button.edit.label"
                                default="Edit"/></g:link>
                        <g:link class="btn btn-danger"
                                url="[action: 'deleteTask', id: courseInstance.id, params: [taskId: taskInstance.id]]"
                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
                            ${message(code: 'default.button.delete.label', default: 'Delete')}
                        </g:link>
                    </div>
                </membership:teacherOrAdmin>
            </div>
        </div>

    </g:each>
</div>

<membership:teacherOrAdmin>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title"><g:message code="default.create.label" args="['Homework']"/></h3>
        </div>
        <g:form url="[action: 'saveTask', id: courseInstance.id]">
            <div class="panel-body">
                <g:hasErrors bean="${newTask}">
                    <ul class="list-group">
                        <g:eachError bean="${newTask}" var="error">
                            <li <g:if test="${error in FieldError}">class="list-group-item list-group-item-danger"</g:if>><g:message
                                    error="${error}"/></li>
                        </g:eachError>
                    </ul>
                </g:hasErrors>

                <g:hiddenField name="course.id" value="${courseInstance.id}"/>
                <g:hiddenField name="type" value="${Type.HOMEWORK.name()}"/>

                <div class="form-group ${hasErrors(bean: newTask, field: 'number', 'error')} ">
                    <label class="control-label" for="number">
                        <g:message code="task.number.label" default="Number"/>
                    </label>
                    <g:field class="form-control" name="number" type="number" value="${newTask.number}"/>
                </div>

                <div class="form-group ${hasErrors(bean: newTask, field: 'name', 'error')}">
                    <label class="control-label" for="name">
                        <g:message code="task.name.label" default="Name"/>
                    </label>
                    <g:textField class="form-control" name="name" maxlength="100" required="" value="${newTask?.name}"/>
                </div>

                <div class="form-group ${hasErrors(bean: newTask, field: 'description', 'error')} ">
                    <label class="control-label" for="description">
                        <g:message code="task.description.label" default="Description"/>
                    </label>
                    <ckeditor:editor name="description">${newTask.description}</ckeditor:editor>
                </div>

                <div class="form-group ${hasErrors(bean: newTask, field: 'color', 'error')} ">
                    <label class="control-label" for="color">
                        <g:message code="task.description.label" default="Color"/>
                    </label>

                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default active">
                            <input type="radio" name="color" id="option1" autocomplete="off"
                                   value="${Color.DEFAULT.name()}" checked> Default
                        </label>
                        <label class="btn btn-primary">
                            <input type="radio" name="color" id="option2" autocomplete="off"
                                   value="${Color.PRIMARY.name()}"> Primary
                        </label>
                        <label class="btn btn-success">
                            <input type="radio" name="color" id="option3" autocomplete="off"
                                   value="${Color.SUCCESS.name()}"> Success
                        </label>
                        <label class="btn btn-info">
                            <input type="radio" name="color" id="option4" autocomplete="off"
                                   value="${Color.INFO.name()}"> Info
                        </label>
                        <label class="btn btn-warning">
                            <input type="radio" name="color" id="option5" autocomplete="off"
                                   value="${Color.WARNING.name()}"> Warning
                        </label>
                        <label class="btn btn-danger">
                            <input type="radio" name="color" id="option6" autocomplete="off"
                                   value="${Color.DANGER.name()}"> Danger
                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group ">
                            <span class="input-group-addon">
                                <g:checkBox name="isVisible" value="${newTask?.isVisible}"/>
                                Always Visible
                            </span>
                            <span class="input-group-addon">
                                <g:checkBox name="visibleOnTime" value="${newTask?.visibleOnTime}"/>
                                Visible on time
                            </span>
                            <span class="input-group-addon">
                                <g:datePicker class="form-control" name="date" precision="minute" value="${newTask?.date}"/>
                            </span>
                        </div><!-- /input-group --></div>
                </div>

            </div>

            <div class="panel-footer">
                <g:submitButton class="btn btn-success" name="create"
                                value="${message(code: 'default.button.create.label', default: 'Create')}"/>
            </div>
        </g:form>
    </div>
</membership:teacherOrAdmin>
</body>
</html>