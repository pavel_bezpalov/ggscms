<%@ page import="ggscms.Course" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'course.label', default: 'Course')}"/>
    <title>GGSCMS</title>
    <style type="text/css">
    .navbar-brand {
        padding: 5px 5px;
    }
    </style>
</head>

<body>
<content tag="navbar">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><asset:image src="grails_logo.png" alt="Grails"/></a>
            </div>

            <div id="navbar" class="navbar-collapse collapse navbar-right">
                <g:render template="/common/topbar"/>
            </div><!--/.navbar-collapse -->
        </div>
    </nav>
</content>

<div class="row row-offcanvas row-offcanvas-right">

    <div class="col-xs-12 col-sm-12">
        <g:if test="${flash.message}">
            <div class="alert alert-info" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                ${flash.message}</div>
        </g:if>
        <!--<p class="pull-right visible-xs">
        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
    </p>-->

    <g:each in="${courseInstanceList}" status="i" var="courseInstance">
        <membership:teacher courseId="${courseInstance.id}">
            <div class="col-xs-6 col-lg-3">
            <h2><g:link action="chapters"
                        id="${courseInstance.id}">${fieldValue(bean: courseInstance, field: "name")}</g:link></h2>
        </membership:teacher>
        <membership:student courseId="${courseInstance.id}">
            <div class="col-xs-6 col-lg-3">
            <h2><g:link action="chapters"
                        id="${courseInstance.id}">${fieldValue(bean: courseInstance, field: "name")}</g:link></h2>
        </membership:student>
        <membership:listener courseId="${courseInstance.id}">
            <div class="col-xs-6 col-lg-3">
            <h2><g:link action="chapters"
                        id="${courseInstance.id}">${fieldValue(bean: courseInstance, field: "name")}</g:link></h2>
        </membership:listener>
        <membership:subscribed courseId="${courseInstance.id}">
            <div class="col-xs-6 col-lg-3">
            <h2>${fieldValue(bean: courseInstance, field: "name")}</h2>
        </membership:subscribed>
        <membership:none courseId="${courseInstance.id}">
            <shiro:hasRole name="administrator">
                <div class="col-xs-6 col-lg-3">
                    <h2><g:link action="chapters"
                        id="${courseInstance.id}">${fieldValue(bean: courseInstance, field: "name")}</g:link></h2>
            </shiro:hasRole>
            <shiro:lacksRole name="administrator">
                <div class="col-xs-6 col-lg-3">
                <h2>${fieldValue(bean: courseInstance, field: "name")}</h2>
            </shiro:lacksRole>
        </membership:none>
        <shiro:notUser>
            <div class="col-xs-6 col-lg-3">
            <h2>${fieldValue(bean: courseInstance, field: "name")}</h2>
        </shiro:notUser>

        <p>${fieldValue(bean: courseInstance, field: "shortDescription")}</p>

        <p><button type="button" class="btn btn-default" data-toggle="modal" data-target="#courseModal${courseInstance.id}">
            More info
        </button></p>

        <!-- Modal -->
        <div class="modal fade" id="courseModal${courseInstance.id}" tabindex="-1" role="dialog" aria-labelledby="courseModal${courseInstance.id}Label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="courseModal${courseInstance.id}Label">${fieldValue(bean: courseInstance, field: "name")}</h4>
                    </div>
                    <div class="modal-body">
                        ${raw(courseInstance.description)}
                    </div>
                    <div class="modal-footer">
        <shiro:hasAnyRole in="['administrator']">
                        <g:link class="btn btn-warning" action="edit" resource="${courseInstance}"><g:message
                                code="default.button.edit.label"
                                default="Edit"/></g:link>
                        <g:link class="btn btn-danger" action="delete" resource="${courseInstance}"
                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
                            ${message(code: 'default.button.delete.label', default: 'Delete')}
                        </g:link>
        </shiro:hasAnyRole>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="${courseInstance.id}">
            <membership:teacher courseId="${courseInstance.id}">
                <p><button type="button" class="btn btn-danger" disabled="disabled">You are a teacher!</button></p>
            </membership:teacher>
            <membership:student courseId="${courseInstance.id}">
                <p><button type="button" class="btn btn-warning" disabled="disabled">You are a student!</button></p>
            </membership:student>
            <membership:listener courseId="${courseInstance.id}">
                <p><button type="button" class="btn btn-success" disabled="disabled">You are a listener!</button></p>
            </membership:listener>
            <membership:subscribed courseId="${courseInstance.id}">

                <p><g:remoteLink class="btn btn-info" action="subscribe" id="${courseInstance.id}"
                                 update="${courseInstance.id}">Unsubscribe now!</g:remoteLink></p>

            </membership:subscribed>
            <membership:none courseId="${courseInstance.id}">

                <p><g:remoteLink class="btn btn-primary" action="subscribe" id="${courseInstance.id}"
                                 update="${courseInstance.id}">Subscribe now!</g:remoteLink></p>

            </membership:none>
        <!-- Button trigger modal -->
        </div>
        </div><!--/.col-xs-6.col-lg-4-->

    </g:each>

</div><!--/.col-xs-12.col-sm-9-->
</div><!--/row-->
    <shiro:hasRole name="administrator">
        <nav class="navbar navbar-inverse navbar-fixed-bottom">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#courses-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="courses-collapse">
                <ul class="nav navbar-nav">
                    <g:link class="btn btn-success navbar-btn" action="create"><g:message code="default.new.label"
                                                                                              args="[entityName]"/></g:link>
                </ul>


                <ul class="nav navbar-nav navbar-right">
                    <g:link class="btn btn-primary navbar-btn" controller="user" action="index">All Users</g:link>
                </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </nav>
    </shiro:hasRole>
</body>
</html>
