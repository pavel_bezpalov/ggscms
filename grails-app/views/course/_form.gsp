<%@ page import="ggscms.Course" %>

<div class="form-group ${hasErrors(bean: courseInstance, field: 'name', 'error')}">
	<label class="control-label" for="name">
		<g:message code="course.name.label" default="Name" />
	</label>
	<g:textField class="form-control" name="name" maxlength="100" required="" value="${courseInstance?.name}"/>
</div>

<div class="form-group ${hasErrors(bean: courseInstance, field: 'shortDescription', 'error')}">
	<label class="control-label" for="shortDescription">
		<g:message code="course.description.label" default="Short Description" />
	</label>
	<g:textArea class="form-control" name="shortDescription" rows="3" required="" value="${courseInstance.shortDescription}"/>
</div>

<div class="form-group ${hasErrors(bean: courseInstance, field: 'description', 'error')}">
	<label class="control-label" for="description">
		<g:message code="course.description.label" default="Description" />
	</label>
	<ckeditor:editor name="description">${courseInstance.description}</ckeditor:editor>
</div>
