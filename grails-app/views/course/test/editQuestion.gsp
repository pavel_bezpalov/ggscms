<%@ page import="org.springframework.validation.FieldError; ggscms.Question" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'question.label', default: 'Question')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<content tag="navbar">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><g:fieldValue bean="${courseInstance}" field="name"/></a>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><g:link action="chapters" id="${courseInstance.id}">Course program</g:link></li>
                    <li><g:link action="lessons" id="${courseInstance.id}">Lessons</g:link></li>
                    <li><g:link action="homeworks" id="${courseInstance.id}">Homeworks</g:link></li>
                    <li class="active"><g:link action="tests" id="${courseInstance.id}">Tests</g:link></li>
                    <li><g:link action="rating" id="${courseInstance.id}">Rating</g:link></li>
                    <membership:teacherOrAdmin>
                        <li><g:link action="users" id="${courseInstance.id}">Users</g:link></li>
                    </membership:teacherOrAdmin>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <g:render template="/common/topbar"/>
                </ul>
            </div><!--/.navbar-collapse -->
        </div>
    </nav>
</content>

<g:if test="${flash.message}">
    <div class="alert alert-info" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        ${flash.message}</div>
</g:if>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <g:each in="${questionInstance.test.questions.sort() { it.id }}" status="i" var="questionFromList">
        <g:if test="${questionInstance.id == questionFromList.id}">
            <g:form url="[action: 'updateQuestion', id: courseInstance.id, params: [questionId: questionInstance.id]]">
                <div class="panel panel-warning">
                    <div class="panel-heading" role="tab" id="collapseListGroupHeading${i}">
                        <h4 id="-collapsible-list-group-" class="panel-title">
                            <a class="" data-toggle="collapse" href="#collapseListGroup${i}" aria-expanded="true"
                               aria-controls="collapseListGroup${i}">
                                <h3 class="panel-title">
                                    EDIT ${fieldValue(bean: questionInstance, field: "name")}
                                </h3>
                            </a>
                            <a class="anchorjs-link" href="#-collapsible-list-group-"><span
                                    class="anchorjs-icon"></span></a></h4>
                    </div>

                    <div style="" aria-expanded="true" id="collapseListGroup${i}" class="panel-collapse collapse in"
                         role="tabpanel" aria-labelledby="collapseListGroupHeading${i}">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <g:hasErrors bean="${questionInstance}">
                                    <ul class="list-group">
                                        <g:eachError bean="${questionInstance}" var="error">
                                            <li <g:if test="${error in FieldError}">class="list-group-item list-group-item-danger"</g:if>><g:message
                                                    error="${error}"/></li>
                                        </g:eachError>
                                    </ul>
                                </g:hasErrors>

                                <div class="form-group ${hasErrors(bean: questionInstance, field: 'name', 'error')}">
                                    <label class="control-label" for="name">
                                        <g:message code="test.name.label" default="Name"/>
                                    </label>
                                    <g:textField class="form-control" name="name" maxlength="100" required=""
                                                 value="${questionInstance?.name}"/>
                                </div>

                                <div class="form-group ${hasErrors(bean: questionInstance, field: 'description', 'error')} ">
                                    <label class="control-label" for="description">
                                        <g:message code="test.description.label" default="Description"/>
                                    </label>
                                    <ckeditor:editor
                                            name="description">${questionInstance.description}</ckeditor:editor>
                                </div>

                                <g:if test="${questionInstance?.variants}">
                                    <g:set var="variantCount" value="${questionInstance.variants.size()}"/>
                                    <g:each in="${questionInstance.variants.sort() { it.id }}" status="z"
                                            var="variantInstance">
                                        <g:hiddenField name="variantId${z + 1}" value="${variantInstance.id}"/>

                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <input name="variantCorrect${z + 1}" type="checkbox"
                                                    ${variantInstance.correct ? 'checked' : ''}>
                                                <g:message code="variantCorrect1.label" default="Correct"/>
                                            </span>
                                            <g:textField class="form-control" name="variantName${z + 1}"
                                                         value="${variantInstance.name}" maxlength="100"/>
                                        </div><!-- /input-group -->

                                    </g:each>

                                    <g:while test="${variantCount < 5}">
                                        <% variantCount++ %>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <input name="variantCorrect${variantCount}" type="checkbox">
                                                <g:message code="variantCorrect1.label" default="Correct"/>
                                            </span>
                                            <g:textField class="form-control" name="variantName${variantCount}"
                                                         maxlength="100" placeholder="NewVariant ${variantCount}"/>
                                        </div><!-- /input-group -->
                                    </g:while>

                                </g:if>
                                <g:else>
                                    <g:set var="variantCount" value="${0}"/>
                                    <g:while test="${variantCount < 5}">
                                        <% variantCount++ %>

                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <input name="variantCorrect${variantCount}" type="checkbox">
                                                <g:message code="variantCorrect1.label" default="Correct"/>
                                            </span>
                                            <g:textField class="form-control" name="variantName${variantCount}"
                                                         maxlength="100" placeholder="NewVariant ${variantCount}"/>
                                        </div><!-- /input-group -->

                                    </g:while>
                                </g:else>

                            </li>
                        </ul>

                        <div class="panel-footer">
                            <g:actionSubmit class="btn btn-warning" action="updateQuestion"
                                            value="${message(code: 'default.button.update.label', default: 'Update')}"/>
                            <g:link class="btn btn-danger"
                                    url="[action: 'deleteQuestion', id: courseInstance.id, params: [questionId: questionInstance.id]]"
                                    onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
                                ${message(code: 'default.button.delete.label', default: 'Delete')}
                            </g:link>
                        </div>

                    </div>
                </div>
            </g:form>
        </g:if>
        <g:else>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="collapseListGroupHeading${i}">
                    <h4 id="-collapsible-list-group-" class="panel-title">
                        <a class="" data-toggle="collapse" href="#collapseListGroup${i}" aria-expanded="true"
                           aria-controls="collapseListGroup${i}">
                            <h3 class="panel-title">
                                ${fieldValue(bean: questionInstance, field: "name")}
                            </h3>
                        </a>
                        <a class="anchorjs-link" href="#-collapsible-list-group-"><span class="anchorjs-icon"></span>
                        </a>
                    </h4>
                </div>

                <div style="" aria-expanded="true" id="collapseListGroup${i}" class="panel-collapse collapse"
                     role="tabpanel" aria-labelledby="collapseListGroupHeading${i}">
                    <ul class="list-group">
                        <li class="list-group-item">
                            ${raw(questionInstance.description)}
                        </li>
                        <g:each in="${questionInstance.variants.sort() { it.id }}" status="y" var="variantInstance">
                            <li class="list-group-item">
                                <g:if test="${variantInstance.correct}">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="optionsRadios${i}${y}" id="optionsRadios${i}${y}"
                                                   checked>
                                            ${variantInstance.name}
                                        </label>
                                    </div>
                                </g:if>
                                <g:else>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="${i}${y}optionsRadios" id="${i}${y}optionsRadios"
                                                   disabled>
                                            <s>${variantInstance.name}</s>
                                        </label>
                                    </div>
                                </g:else>
                            </li>
                        </g:each>
                    </ul>

                    <div class="panel-footer">
                        <g:link class="btn btn-warning" action="editQuestion"
                                id="${courseInstance.id}"
                                params="[questionId: questionInstance.id]"><g:message
                                code="default.button.edit.label"
                                default="Edit"/></g:link>
                        <g:link class="btn btn-danger"
                                url="[action: 'deleteQuestion', id: courseInstance.id, params: [questionId: questionInstance.id]]"
                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
                            ${message(code: 'default.button.delete.label', default: 'Delete')}
                        </g:link>
                    </div>
                </div>
            </div>
        </g:else>
    </g:each>
</div>

</body>
</html>
