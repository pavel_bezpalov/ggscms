<%@ page import="ggscms.Answer" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'test.label', default: 'Test')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
    <style type="text/css">
    body { padding-top: 10px; }
    </style>
    <g:javascript>
        var time = '${timeLeft.toInteger()}';

        $(document).ready(function() {
            setTimeout(function() {
                $("#submit").click();
            }, time*1000);
        });

        $(document).ready(function() {
         $('#countdown-1').timeTo({
         seconds: ${timeLeft.toInteger()},
         displayCaptions: true,
         theme: "black"
         });
        });

    </g:javascript>
</head>

<body>
<content tag="navbar">
</content>

<g:if test="${flash.message}">
    <div class="alert alert-info" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        ${flash.message}</div>
</g:if>

<g:form name="testingForm" url="[action: 'submitTest', id: courseInstance.id, params: [testId: testInstance.id]]">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        <g:each in="${testInstance.questions.sort() { it.id }}" status="i" var="questionInstance">
            <g:hiddenField name="questionId${i + 1}" value="${questionInstance.id}"/>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="collapseListGroupHeading${i}">
                    <h4 id="-collapsible-list-group-" class="panel-title">
                        <a class="" data-toggle="collapse" href="#collapseListGroup${i}" aria-expanded="true"
                           aria-controls="collapseListGroup${i}">
                            <h3 class="panel-title">
                                <strong>${fieldValue(bean: questionInstance, field: "name")}</strong>
                            </h3>
                        </a>
                        <a class="anchorjs-link" href="#-collapsible-list-group-"><span class="anchorjs-icon"></span>
                        </a>
                    </h4>
                </div>

                <div style="" aria-expanded="true" id="collapseListGroup${i}" class="panel-collapse collapse in"
                     role="tabpanel" aria-labelledby="collapseListGroupHeading${i}">
                    <ul class="list-group">
                        <g:if test="${questionInstance.description}">
                            <li class="list-group-item">
                                ${raw(questionInstance.description)}
                            </li>
                        </g:if>
                        <g:each in="${questionInstance.variants.sort() { it.id }}" status="y" var="variantInstance">
                            <g:hiddenField name="variantId${i + 1}${y + 1}" value="${variantInstance.id}"/>
                            <li class="list-group-item">

                                    <div class="checkbox">
                                        <label class="control-label" for="variantAnswer${i + 1}${y + 1}">
                                            <g:if test="${variantInstance in Answer.findByUserAndTestAndQuestion(user, testInstance, questionInstance)?.variants}">
                                                <g:checkBox name="variantAnswer${i + 1}${y + 1}" value="${true}"/>
                                            </g:if>
                                            <g:else>
                                                <g:checkBox name="variantAnswer${i + 1}${y + 1}"/>
                                            </g:else>

                                            ${variantInstance.name}
                                        </label>
                                    </div>

                            </li>
                        </g:each>
                    </ul>
                </div>
            </div>
        </g:each>

    </div>

    <nav class="navbar navbar-inverse navbar-fixed-bottom">
        <div class="container">
            <div class="navbar-header">
            </div>
            <ul class="nav navbar-nav">
                <li><g:submitButton name="submit" class="btn btn-danger navbar-btn"
                                    value="${message(code: 'default.button.submitTest.label', default: 'Submit Test')}"/>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><div id="countdown-1"></div></li>
            </ul>
        </div>
    </nav>
</g:form>
<content tag="footer">
</content>
</body>
</html>
