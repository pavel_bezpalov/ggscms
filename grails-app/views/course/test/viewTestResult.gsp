<%@ page import="ggscms.Answer" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'test.label', default: 'Test')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<content tag="navbar">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><g:fieldValue bean="${courseInstance}" field="name"/></a>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><g:link action="chapters" id="${courseInstance.id}">Course program</g:link></li>
                    <li><g:link action="lessons" id="${courseInstance.id}">Lessons</g:link></li>
                    <li><g:link action="homeworks" id="${courseInstance.id}">Homeworks</g:link></li>
                    <li class="active"><g:link action="tests" id="${courseInstance.id}">Tests</g:link></li>
                    <li><g:link action="rating" id="${courseInstance.id}">Rating</g:link></li>
                    <membership:teacherOrAdmin>
                        <li><g:link action="users" id="${courseInstance.id}">Users</g:link></li>
                    </membership:teacherOrAdmin>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <g:render template="/common/topbar"/>
                </ul>
            </div><!--/.navbar-collapse -->
        </div>
    </nav>
</content>

<g:if test="${flash.message}">
    <div class="alert alert-info" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        ${flash.message}</div>
</g:if>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <g:each in="${testInstance.questions.sort() { it.id }}" status="i" var="questionInstance">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="collapseListGroupHeading${i}">
                <h4 id="-collapsible-list-group-" class="panel-title">
                    <a class="" data-toggle="collapse" href="#collapseListGroup${i}" aria-expanded="true"
                       aria-controls="collapseListGroup${i}">
                        <h3 class="panel-title">
                            ${fieldValue(bean: questionInstance, field: "name")}
                        </h3>
                    </a>
                    <a class="anchorjs-link" href="#-collapsible-list-group-"><span class="anchorjs-icon"></span>
                    </a>
                </h4>
            </div>
            <div style="" aria-expanded="true" id="collapseListGroup${i}" class="panel-collapse collapse in"
                 role="tabpanel" aria-labelledby="collapseListGroupHeading${i}">
                <ul class="list-group">
                    <g:if test="${questionInstance.description}">
                        <li class="list-group-item">
                            ${raw(questionInstance.description)}
                        </li>
                    </g:if>
                    <g:each in="${questionInstance.variants.sort() { it.id }}" status="y" var="variantInstance">
                        <g:if test="${variantInstance in Answer.findByUserAndTestAndQuestion(user, testInstance, questionInstance)?.variants}">
                            <g:if test="${variantInstance.correct}">
                                <li class="list-group-item list-group-item-success">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="optionsRadios${i}${y}" id="optionsRadios${i}${y}"
                                                   checked>
                                            ${variantInstance.name}
                                        </label>
                                    </div>
                                </li>
                            </g:if>
                            <g:else>
                                <li class="list-group-item list-group-item-danger">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="optionsRadios${i}${y}" id="optionsRadios${i}${y}"
                                                   checked>
                                            <s>${variantInstance.name}</s>
                                        </label>
                                    </div>
                                </li>
                            </g:else>
                        </g:if>
                        <g:else>
                            <g:if test="${variantInstance.correct}">
                                <li class="list-group-item list-group-item-danger">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="optionsRadios${i}${y}" id="optionsRadios${i}${y}"
                                                   disabled>
                                            ${variantInstance.name}
                                        </label>
                                    </div>
                                </li>
                            </g:if>
                            <g:else>
                                <li class="list-group-item list-group-item-success">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="optionsRadios${i}${y}" id="optionsRadios${i}${y}"
                                                   disabled>
                                            <s>${variantInstance.name}</s>
                                        </label>
                                    </div>
                                </li>
                            </g:else>
                        </g:else>
                    </g:each>
                </ul>
            </div>
        </div>
    </g:each>
</div>

</body>
</html>
