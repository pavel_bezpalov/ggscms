<%@ page import="org.springframework.validation.FieldError; ggscms.Color; ggscms.Test" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'test.label', default: 'Test')}" />
    <title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>

<content tag="navbar">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><g:fieldValue bean="${courseInstance}" field="name"/></a>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><g:link action="chapters" id="${courseInstance.id}">Course program</g:link></li>
                    <li><g:link action="lessons" id="${courseInstance.id}">Lessons</g:link></li>
                    <li><g:link action="homeworks" id="${courseInstance.id}">Homeworks</g:link></li>
                    <li class="active"><g:link action="tests" id="${courseInstance.id}">Tests</g:link></li>
                    <li><g:link action="rating" id="${courseInstance.id}">Rating</g:link></li>
                    <membership:teacherOrAdmin>
                        <li><g:link action="users" id="${courseInstance.id}">Users</g:link></li>
                    </membership:teacherOrAdmin>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <g:render template="/common/topbar"/>
                </ul>
            </div><!--/.navbar-collapse -->
        </div>
    </nav>
</content>

<g:if test="${flash.message}">
    <div class="alert alert-info" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        ${flash.message}</div>
</g:if>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <g:each in="${tests}" status="i" var="testFromList">
        <g:if test="${testInstance.id == testFromList.id}">
            <g:form url="[action: 'updateTest', id: courseInstance.id, params: [testId: testInstance.id]]">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="collapseListGroupHeading${i}">
                        <h4 id="-collapsible-list-group-" class="panel-title">
                            <a class="" data-toggle="collapse" href="#collapseListGroup${i}" aria-expanded="true"
                               aria-controls="collapseListGroup${i}">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-8 col-sm-8">
                                            EDIT <strong>${fieldValue(bean: testInstance, field: "number")}</strong>
                                            ${fieldValue(bean: testInstance, field: "name")}
                                        </div>

                                        <div class="col-xs-4 col-sm-4 text-right">
                                            <membership:teacherOrAdmin>
                                                <g:if test="${testInstance.isVisible}">
                                                    Always VISIBLE
                                                </g:if>
                                                <g:elseif
                                                        test="${!testInstance.isVisible && testInstance.visibleOnTime && testInstance.date < new Date()}">
                                                    Already VISIBLE since <g:formatDate format="HH:mm, dd MMMM yyyy"
                                                                                        date="${testInstance.date}"/>
                                                </g:elseif>
                                                <g:elseif
                                                        test="${!testInstance.isVisible && testInstance.visibleOnTime && testInstance.date > new Date()}">
                                                    INVISIBLE till <g:formatDate format="HH:mm, dd MMMM yyyy"
                                                                                 date="${testInstance.date}"/>
                                                </g:elseif>
                                                <g:else>
                                                    Always INVISIBLE
                                                </g:else>
                                            </membership:teacherOrAdmin>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="anchorjs-link" href="#-collapsible-list-group-"><span
                                    class="anchorjs-icon"></span></a></h4>
                    </div>

                    <div style="" aria-expanded="true" id="collapseListGroup${i}" class="panel-collapse collapse in"
                         role="tabpanel" aria-labelledby="collapseListGroupHeading${i}">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <g:hasErrors bean="${testInstance}">
                                    <ul class="list-group">
                                        <g:eachError bean="${testInstance}" var="error">
                                            <li <g:if test="${error in FieldError}">class="list-group-item list-group-item-danger"</g:if>><g:message
                                                    error="${error}"/></li>
                                        </g:eachError>
                                    </ul>
                                </g:hasErrors>

                                <div class="form-group ${hasErrors(bean: testInstance, field: 'number', 'error')} ">
                                    <label class="control-label" for="number">
                                        <g:message code="testInstance.number.label" default="Number"/>
                                    </label>
                                    <g:field class="form-control" name="number" type="number"
                                             value="${testInstance.number}"/>
                                </div>

                                <div class="form-group ${hasErrors(bean: testInstance, field: 'timeLimit', 'error')} ">
                                    <label class="control-label" for="timeLimit">
                                        <g:message code="test.timeLimit.label" default="Time Limit"/>
                                    </label>
                                    <g:field class="form-control" name="timeLimit" type="number" value="${testInstance?.timeLimit}" required=""/>
                                </div>

                                <div class="form-group ${hasErrors(bean: testInstance, field: 'name', 'error')}">
                                    <label class="control-label" for="name">
                                        <g:message code="testInstance.name.label" default="Name"/>
                                    </label>
                                    <g:textField class="form-control" name="name" maxlength="100" required=""
                                                 value="${testInstance?.name}"/>
                                </div>

                                <div class="form-group ${hasErrors(bean: testInstance, field: 'description', 'error')} ">
                                    <label class="control-label" for="description">
                                        <g:message code="testInstance.description.label" default="Description"/>
                                    </label>
                                    <ckeditor:editor name="description">${testInstance.description}</ckeditor:editor>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                                <g:checkBox name="isVisible" value="${testInstance?.isVisible}"/>
                                                Always Visible
                                            </span>
                                            <span class="input-group-addon">
                                                <g:checkBox name="visibleOnTime" value="${testInstance?.visibleOnTime}"/>
                                                Visible on time
                                            </span>
                                            <span class="input-group-addon">
                                                <g:datePicker class="form-control" name="date" precision="minute" value="${testInstance?.date}"/>
                                            </span>
                                        </div><!-- /input-group --></div>
                                </div>
                                <div class="form-group ${hasErrors(bean: testInstance, field: 'allowViewResults', 'error')} ">
                                    <div class="checkbox">
                                        <label class="control-label">
                                            <g:checkBox name="allowViewResults" value="${testInstance?.allowViewResults}"/> <g:message
                                                    code="test.allowViewResults.label" default="Allow View Results"/>
                                        </label>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <div class="panel-footer">
                            <g:actionSubmit class="btn btn-warning" action="updateTest"
                                            value="${message(code: 'default.button.update.label', default: 'Update')}"/>
                            <g:link class="btn btn-danger"
                                    url="[action: 'deleteTest', id: courseInstance.id, params: [testId: testInstance.id]]"
                                    onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
                                ${message(code: 'default.button.delete.label', default: 'Delete')}
                            </g:link>
                        </div>

                    </div>
                </div>
            </g:form>
        </g:if>
        <g:else>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="collapseListGroupHeading${i}">
                    <h4 id="-collapsible-list-group-" class="panel-title">
                        <a class="" data-toggle="collapse" href="#collapseListGroup${i}" aria-expanded="true"
                           aria-controls="collapseListGroup${i}">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-8 col-sm-8">
                                        <strong>${fieldValue(bean: testFromList, field: "number")}</strong>
                                        ${fieldValue(bean: testFromList, field: "name")}
                                    </div>

                                    <div class="col-xs-4 col-sm-4 text-right">
                                        <membership:teacherOrAdmin>
                                            <g:if test="${testFromList.isVisible}">
                                                Always VISIBLE
                                            </g:if>
                                            <g:elseif
                                                    test="${!testFromList.isVisible && testFromList.visibleOnTime && testFromList.date < new Date()}">
                                                Already VISIBLE since <g:formatDate format="HH:mm, dd MMMM yyyy"
                                                                                    date="${testFromList.date}"/>
                                            </g:elseif>
                                            <g:elseif
                                                    test="${!testFromList.isVisible && testFromList.visibleOnTime && testFromList.date > new Date()}">
                                                INVISIBLE till <g:formatDate format="HH:mm, dd MMMM yyyy"
                                                                             date="${testFromList.date}"/>
                                            </g:elseif>
                                            <g:else>
                                                Always INVISIBLE
                                            </g:else>
                                        </membership:teacherOrAdmin>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="anchorjs-link" href="#-collapsible-list-group-"><span class="anchorjs-icon"></span></a>
                    </h4>
                </div>

                <div style="" aria-expanded="true" id="collapseListGroup${i}" class="panel-collapse collapse"
                     role="tabpanel" aria-labelledby="collapseListGroupHeading${i}">
                    <ul class="list-group">
                        <li class="list-group-item">${raw(testFromList.description)}</li>
                        <li class="list-group-item">
                            <g:if test="${testFromList.allowViewResults}">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios${i}" id="optionsRadios${i}">
                                        Allow View Results to Users
                                    </label>
                                </div>
                            </g:if>
                            <g:else>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="${i}optionsRadios" id="${i}optionsRadios"
                                               disabled>
                                        <s>Allow View Results to Users</s>
                                    </label>
                                </div>
                            </g:else>
                        </li>
                        <li class="list-group-item">
                            Time limit: ${testFromList.timeLimit} minutes
                        </li>
                    </ul>
                    <membership:teacherOrAdmin>
                        <div class="panel-footer">
                            <g:link class="btn btn-warning" action="editTest"
                                    id="${courseInstance.id}"
                                    params="[testId: testFromList.id]"><g:message
                                    code="default.button.edit.label"
                                    default="Edit"/></g:link>
                            <g:link class="btn btn-danger"
                                    url="[action: 'deleteTest', id: courseInstance.id, params: [testId: testFromList.id]]"
                                    onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
                                ${message(code: 'default.button.delete.label', default: 'Delete')}
                            </g:link>
                        </div>
                    </membership:teacherOrAdmin>
                </div>
            </div>
        </g:else>
    </g:each>
</div>
</body>
</html>
