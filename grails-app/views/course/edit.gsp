<%@ page import="ggscms.Course" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'course.label', default: 'Course')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
    <style type="text/css">
    .navbar-brand {
        padding: 5px 5px;
    }
    </style>
</head>

<body>
<g:if test="${flash.message}">
    <div class="alert alert-info" role="alert">${flash.message}</div>
</g:if>
<div class="panel panel-warning">
    <div class="panel-heading">
        <h3 class="panel-title"><g:message code="default.edit.label" args="[courseInstance.name]"/></h3>
    </div>
    <g:form url="[resource: courseInstance, action: 'update']">
        <div class="panel-body">
            <g:hasErrors bean="${courseInstance}">
                <ul class="list-group">
                    <g:eachError bean="${courseInstance}" var="error">
                        <li <g:if test="${error in org.springframework.validation.FieldError}">class="list-group-item list-group-item-danger"</g:if>><g:message
                                error="${error}"/></li>
                    </g:eachError>
                </ul>
            </g:hasErrors>

            <g:hiddenField name="version" value="${courseInstance?.version}"/>

            <g:render template="form"/>

        </div>
        <div class="panel-footer">
            <g:actionSubmit class="btn btn-warning" action="update"
                            value="${message(code: 'default.button.update.label', default: 'Update')}"/>
        </div>
    </g:form>
</div>
</body>
</html>
