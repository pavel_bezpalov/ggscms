<%@ page import="ggscms.Chapter; ggscms.Course" %>
<%@ page import="ggscms.Membership" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'course.label', default: 'Course')}"/>
    <title><g:fieldValue bean="${courseInstance}"
                         field="name"/></title>
</head>

<body>
<content tag="navbar">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><g:fieldValue bean="${courseInstance}" field="name"/></a>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><g:link action="chapters" id="${courseInstance.id}">Course program</g:link></li>
                    <li><g:link action="lessons" id="${courseInstance.id}">Lessons</g:link></li>
                    <li><g:link action="homeworks" id="${courseInstance.id}">Homeworks</g:link></li>
                    <li><g:link action="tests" id="${courseInstance.id}">Tests</g:link></li>
                    <li><g:link action="rating" id="${courseInstance.id}">Rating</g:link></li>
                    <li class="active"><g:link action="users" id="${courseInstance.id}">Users</g:link></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <g:render template="/common/topbar"/>
                </ul>
            </div><!--/.navbar-collapse -->
        </div>
    </nav>
</content>

<g:if test="${flash.message}">
    <div class="alert alert-info" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        ${flash.message}</div>
</g:if>

<div class="table-responsive">
<table class="table table-striped">
        <thead>
        <tr>

            <g:sortableColumn property="username" action="users" title="${message(code: 'user.username.label', default: 'Username')}" />

            <g:sortableColumn property="fullName" action="users" title="${message(code: 'user.fullName.label', default: 'Full Name')}" />

            <g:sortableColumn property="email" action="users" title="${message(code: 'user.email.label', default: 'Email')}" />

            <g:sortableColumn property="phone" action="users" title="${message(code: 'user.phone.label', default: 'Phone')}" />

            <g:sortableColumn property="dateCreated" action="users" title="${message(code: 'user.dateCreated.label', default: 'Date Created')}" />

            <g:sortableColumn property="membership" action="users" title="${message(code: 'user.dateCreated.label', default: 'Membership')}" />

        </tr>
        </thead>
        <tbody>
        <g:each in="${users}" status="i" var="userInstance">
            <tr>

                <td>${fieldValue(bean: userInstance, field: "username")}</td>

                <td>${fieldValue(bean: userInstance, field: "fullName")}</td>

                <td>${fieldValue(bean: userInstance, field: "email")}</td>

                <td>${fieldValue(bean: userInstance, field: "phone")}</td>

                <td><g:formatDate date="${userInstance.dateCreated}" /></td>

                <td>
                    <div id="${userInstance.id}">
                    <g:render template="userAccess" model="[user: userInstance, course: courseInstance]"/>
                    </div>
                </td>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>
</body>
</html>