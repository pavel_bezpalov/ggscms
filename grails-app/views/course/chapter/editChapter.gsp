<%@ page import="org.springframework.validation.FieldError; ggscms.Color; ggscms.Chapter; ggscms.Course" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'course.label', default: 'Course')}"/>
    <title><g:fieldValue bean="${courseInstance}"
                         field="name"/></title>
</head>

<body>
<content tag="navbar">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><g:fieldValue bean="${courseInstance}" field="name"/></a>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><g:link action="chapters" id="${courseInstance.id}">Course program</g:link></li>
                    <li><g:link action="lessons" id="${courseInstance.id}">Lessons</g:link></li>
                    <li><g:link action="homeworks" id="${courseInstance.id}">Homeworks</g:link></li>
                    <li><g:link action="tests" id="${courseInstance.id}">Tests</g:link></li>
                    <li><g:link action="users" id="${courseInstance.id}">Users</g:link></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <g:render template="/common/topbar"/>
                </ul>
            </div><!--/.navbar-collapse -->
        </div>
    </nav>
</content>

<g:if test="${flash.message}">
    <div class="alert alert-info" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        ${flash.message}</div>
</g:if>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <g:each in="${chapters}" status="i" var="chapterInstance">
        <g:if test="${chapterInstance.id == chapter.id}">
            <g:form url="[action: 'updateChapter', id: courseInstance.id, params: [chapterId: chapter.id]]">
                <div class="panel panel-${chapter.color.value}">
                    <div class="panel-heading" role="tab" id="collapseListGroupHeading${i}">
                        <h4 id="-collapsible-list-group-" class="panel-title">
                            <a class="" data-toggle="collapse" href="#collapseListGroup${i}" aria-expanded="true"
                               aria-controls="collapseListGroup${i}">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-9 col-sm-9">
                                            EDIT <strong>${fieldValue(bean: chapter, field: "number")}</strong>
                                            ${fieldValue(bean: chapter, field: "name")}
                                        </div>

                                        <div class="col-xs-3 col-sm-3 text-right">
                                            <g:formatDate format="dd MMMM yyyy"
                                                          date="${chapterInstance.date}"/>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="anchorjs-link" href="#-collapsible-list-group-"><span
                                    class="anchorjs-icon"></span></a></h4>
                    </div>

                    <div style="" aria-expanded="true" id="collapseListGroup${i}" class="panel-collapse collapse in"
                         role="tabpanel" aria-labelledby="collapseListGroupHeading${i}">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <g:hasErrors bean="${chapter}">
                                    <ul class="list-group">
                                        <g:eachError bean="${chapter}" var="error">
                                            <li <g:if test="${error in FieldError}">class="list-group-item list-group-item-danger"</g:if>><g:message
                                                    error="${error}"/></li>
                                        </g:eachError>
                                    </ul>
                                </g:hasErrors>

                                <div class="form-group ${hasErrors(bean: chapter, field: 'number', 'error')} ">
                                    <label class="control-label" for="number">
                                        <g:message code="chapter.number.label" default="Number"/>
                                    </label>
                                    <g:field class="form-control" name="number" type="number"
                                             value="${chapter.number}"/>
                                </div>

                                <div class="form-group ${hasErrors(bean: chapter, field: 'date', 'error')} ">
                                    <label class="control-label" for="date">
                                        <g:message code="chapter.date.label" default="Date"/>
                                    </label>
                                    <g:datePicker class="form-control" name="date" precision="day"
                                                  value="${chapter?.date}"/>
                                </div>

                                <div class="form-group ${hasErrors(bean: chapter, field: 'name', 'error')}">
                                    <label class="control-label" for="name">
                                        <g:message code="chapter.name.label" default="Name"/>
                                    </label>
                                    <g:textField class="form-control" name="name" maxlength="100" required=""
                                                 value="${chapter?.name}"/>
                                </div>

                                <div class="form-group ${hasErrors(bean: chapter, field: 'description', 'error')} ">
                                    <label class="control-label" for="description">
                                        <g:message code="chapter.description.label" default="Description"/>
                                    </label>
                                    <ckeditor:editor name="description">${chapter.description}</ckeditor:editor>
                                </div>

                                <div class="form-group ${hasErrors(bean: chapter, field: 'color', 'error')} ">
                                    <label class="control-label" for="color">
                                        <g:message code="chapter.description.label" default="Color"/>
                                    </label>

                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default active">
                                            <input type="radio" name="color" id="option1" autocomplete="off"
                                                   value="${Color.DEFAULT.name()}" ${chapter.color.name() == "DEFAULT" ? 'checked' : ''}> Default
                                        </label>
                                        <label class="btn btn-primary">
                                            <input type="radio" name="color" id="option2" autocomplete="off"
                                                   value="${Color.PRIMARY.name()}" ${chapter.color.name() == "PRIMARY" ? 'checked' : ''}> Primary
                                        </label>
                                        <label class="btn btn-success">
                                            <input type="radio" name="color" id="option3" autocomplete="off"
                                                   value="${Color.SUCCESS.name()}" ${chapter.color.name() == "SUCCESS" ? 'checked' : ''}> Success
                                        </label>
                                        <label class="btn btn-info">
                                            <input type="radio" name="color" id="option4" autocomplete="off"
                                                   value="${Color.INFO.name()}" ${chapter.color.name() == "INFO" ? 'checked' : ''}> Info
                                        </label>
                                        <label class="btn btn-warning">
                                            <input type="radio" name="color" id="option5" autocomplete="off"
                                                   value="${Color.WARNING.name()}" ${chapter.color.name() == "WARNING" ? 'checked' : ''}> Warning
                                        </label>
                                        <label class="btn btn-danger">
                                            <input type="radio" name="color" id="option6" autocomplete="off"
                                                   value="${Color.DANGER.name()}" ${chapter.color.name() == "DANGER" ? 'checked' : ''}> Danger
                                        </label>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <div class="panel-footer">
                            <g:actionSubmit class="btn btn-warning" action="updateChapter"
                                            value="${message(code: 'default.button.update.label', default: 'Update')}"/>
                            <g:link class="btn btn-danger"
                                    url="[action: 'deleteChapter', id: courseInstance.id, params: [chapterId: chapter.id]]"
                                    onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
                                ${message(code: 'default.button.delete.label', default: 'Delete')}
                            </g:link>
                        </div>

                    </div>
                </div>
            </g:form>
        </g:if>
        <g:else>
            <div class="panel panel-${chapterInstance.color.value}">
                <div class="panel-heading" role="tab" id="collapseListGroupHeading${i}">
                    <h4 id="-collapsible-list-group-" class="panel-title">
                        <a class="" data-toggle="collapse" href="#collapseListGroup${i}" aria-expanded="true"
                           aria-controls="collapseListGroup${i}">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-9 col-sm-9">
                                        <strong>${fieldValue(bean: chapterInstance, field: "number")}</strong>
                                        ${fieldValue(bean: chapterInstance, field: "name")}
                                    </div>

                                    <div class="col-xs-3 col-sm-3 text-right">
                                        <g:formatDate format="dd MMMM yyyy"
                                                      date="${chapterInstance.date}"/>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="anchorjs-link" href="#-collapsible-list-group-"><span class="anchorjs-icon"></span>
                        </a>
                    </h4>
                </div>

                <div style="" aria-expanded="true" id="collapseListGroup${i}" class="panel-collapse collapse"
                     role="tabpanel" aria-labelledby="collapseListGroupHeading${i}">
                    <ul class="list-group">
                        <li class="list-group-item">${raw(chapterInstance.description)}</li>
                    </ul>

                    <div class="panel-footer">
                        <g:link class="btn btn-warning" action="editChapter"
                                id="${courseInstance.id}"
                                params="[chapterId: chapterInstance.id]"><g:message
                                code="default.button.edit.label"
                                default="Edit"/></g:link>
                        <g:link class="btn btn-danger"
                                url="[action: 'deleteChapter', id: courseInstance.id, params: [chapterId: chapterInstance.id]]"
                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
                            ${message(code: 'default.button.delete.label', default: 'Delete')}
                        </g:link>
                    </div>
                </div>
            </div>
        </g:else>
    </g:each>
</div>

</body>
</html>

