<%@ page import="ggscms.Color; ggscms.Type; org.springframework.validation.FieldError; ggscms.Task" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'task.label', default: 'Task')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
<body>
<content tag="navbar">
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
						aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/"><g:fieldValue bean="${courseInstance}" field="name"/></a>
			</div>

			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><g:link action="chapters" id="${courseInstance.id}">Course program</g:link></li>
					<g:if test="${taskInstance.type.name() == "LESSON"}">
						<li class="active"><g:link action="lessons" id="${courseInstance.id}">Lessons</g:link></li>
						<li><g:link action="homeworks" id="${courseInstance.id}">Homeworks</g:link></li>
					</g:if>
					<g:elseif test="${taskInstance.type.name() == "HOMEWORK"}">
						<li><g:link action="lessons" id="${courseInstance.id}">Lessons</g:link></li>
						<li class="active"><g:link action="homeworks" id="${courseInstance.id}">Homeworks</g:link></li>
					</g:elseif>
					<li><g:link action="tests" id="${courseInstance.id}">Tests</g:link></li>
					<li><g:link action="users" id="${courseInstance.id}">Users</g:link></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<g:render template="/common/topbar"/>
				</ul>
			</div><!--/.navbar-collapse -->
		</div>
	</nav>
</content>

<g:if test="${flash.message}">
	<div class="alert alert-info" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		</button>
		${flash.message}</div>
</g:if>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<g:each in="${tasks}" status="i" var="taskFromList">
		<g:if test="${taskFromList.id == taskInstance.id}">
			<g:form url="[action: 'updateTask', id: courseInstance.id, params: [taskId: taskInstance.id]]">
				<div class="panel panel-${taskInstance.color.value}">
					<div class="panel-heading" role="tab" id="collapseListGroupHeading${i}">
						<h4 id="-collapsible-list-group-" class="panel-title">
							<a class="" data-toggle="collapse" href="#collapseListGroup${i}" aria-expanded="true"
							   aria-controls="collapseListGroup${i}">
								<h3 class="panel-title">
									<div class="container-fluid">
									<div class="row">
										<div class="col-xs-8 col-sm-8">
											EDIT <strong>${fieldValue(bean: taskInstance, field: "number")}</strong>
											${fieldValue(bean: taskInstance, field: "name")}
										</div>

										<div class="col-xs-4 col-sm-4 text-right">
											<membership:teacherOrAdmin>
												<g:if test="${taskInstance.isVisible}">
													Always VISIBLE
												</g:if>
												<g:elseif
														test="${!taskInstance.isVisible && taskInstance.visibleOnTime && taskInstance.date < new Date()}">
													Already VISIBLE since <g:formatDate format="HH:mm, dd MMMM yyyy"
																						date="${taskInstance.date}"/>
												</g:elseif>
												<g:elseif
														test="${!taskInstance.isVisible && taskInstance.visibleOnTime && taskInstance.date > new Date()}">
													INVISIBLE till <g:formatDate format="HH:mm, dd MMMM yyyy"
																				 date="${taskInstance.date}"/>
												</g:elseif>
												<g:else>
													Always INVISIBLE
												</g:else>
											</membership:teacherOrAdmin>
										</div>
									</div>
								</div></h3>
							</a>
							<a class="anchorjs-link" href="#-collapsible-list-group-"><span
									class="anchorjs-icon"></span></a></h4>
					</div>

					<div style="" aria-expanded="true" id="collapseListGroup${i}" class="panel-collapse collapse in"
						 role="tabpanel" aria-labelledby="collapseListGroupHeading${i}">
						<ul class="list-group">
							<li class="list-group-item">
								<g:hasErrors bean="${taskInstance}">
									<ul class="list-group">
										<g:eachError bean="${taskInstance}" var="error">
											<li <g:if test="${error in FieldError}">class="list-group-item list-group-item-danger"</g:if>><g:message
													error="${error}"/></li>
										</g:eachError>
									</ul>
								</g:hasErrors>

								<div class="form-group ${hasErrors(bean: taskInstance, field: 'number', 'error')} ">
									<label class="control-label" for="number">
										<g:message code="task.number.label" default="Number"/>
									</label>
									<g:field class="form-control" name="number" type="number"
											 value="${taskInstance.number}"/>
								</div>

								<div class="form-group ${hasErrors(bean: taskInstance, field: 'name', 'error')}">
									<label class="control-label" for="name">
										<g:message code="task.name.label" default="Name"/>
									</label>
									<g:textField class="form-control" name="name" maxlength="100" required=""
												 value="${taskInstance?.name}"/>
								</div>

								<div class="form-group ${hasErrors(bean: taskInstance, field: 'description', 'error')} ">
									<label class="control-label" for="description">
										<g:message code="task.description.label" default="Description"/>
									</label>
									<ckeditor:editor name="description">${taskInstance.description}</ckeditor:editor>
								</div>

								<div class="form-group ${hasErrors(bean: taskInstance, field: 'color', 'error')} ">
									<label class="control-label" for="color">
										<g:message code="task.description.label" default="Color"/>
									</label>

									<div class="btn-group" data-toggle="buttons">
										<label class="btn btn-default active">
											<input type="radio" name="color" id="option1" autocomplete="off"
												   value="${Color.DEFAULT.name()}" ${taskInstance.color.name() == "DEFAULT" ? 'checked': ''}> Default
										</label>
										<label class="btn btn-primary">
											<input type="radio" name="color" id="option2" autocomplete="off"
												   value="${Color.PRIMARY.name()}" ${taskInstance.color.name() == "PRIMARY" ? 'checked': ''}> Primary
										</label>
										<label class="btn btn-success">
											<input type="radio" name="color" id="option3" autocomplete="off"
												   value="${Color.SUCCESS.name()}" ${taskInstance.color.name() == "SUCCESS" ? 'checked': ''}> Success
										</label>
										<label class="btn btn-info">
											<input type="radio" name="color" id="option4" autocomplete="off"
												   value="${Color.INFO.name()}" ${taskInstance.color.name() == "INFO" ? 'checked': ''}> Info
										</label>
										<label class="btn btn-warning">
											<input type="radio" name="color" id="option5" autocomplete="off"
												   value="${Color.WARNING.name()}" ${taskInstance.color.name() == "WARNING" ? 'checked': ''}> Warning
										</label>
										<label class="btn btn-danger">
											<input type="radio" name="color" id="option6" autocomplete="off"
												   value="${Color.DANGER.name()}" ${taskInstance.color.name() == "DANGER" ? 'checked': ''}> Danger
										</label>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6">
										<div class="input-group ">
											<span class="input-group-addon">
												<g:checkBox name="isVisible" value="${taskInstance?.isVisible}"/>
												Always Visible
											</span>
											<span class="input-group-addon">
												<g:checkBox name="visibleOnTime" value="${taskInstance?.visibleOnTime}"/>
												Visible on time
											</span>
											<span class="input-group-addon">
												<g:datePicker class="form-control" name="date" precision="minute" value="${taskInstance?.date}"/>
											</span>
										</div><!-- /input-group --></div>
								</div>

							</li>
						</ul>

						<div class="panel-footer">
							<g:actionSubmit class="btn btn-warning" action="updateTask"
											value="${message(code: 'default.button.update.label', default: 'Update')}"/>
							<g:link class="btn btn-danger"
									url="[action: 'deleteTask', id: courseInstance.id, params: [taskId: taskInstance.id]]"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
								${message(code: 'default.button.delete.label', default: 'Delete')}
							</g:link>
						</div>

					</div>
				</div>
			</g:form>
		</g:if>
		<g:else>
			<div class="panel panel-${taskFromList.color.value}">
				<div class="panel-heading" role="tab" id="collapseListGroupHeading${i}">
					<h4 id="-collapsible-list-group-" class="panel-title">
						<a class="" data-toggle="collapse" href="#collapseListGroup${i}" aria-expanded="true"
						   aria-controls="collapseListGroup${i}">
							<h3 class="panel-title">
								<div class="container-fluid">
								<div class="row">
									<div class="col-xs-8 col-sm-8">
										<strong>${fieldValue(bean: taskFromList, field: "number")}</strong>
										${fieldValue(bean: taskFromList, field: "name")}
									</div>

									<div class="col-xs-4 col-sm-4 text-right">

											<g:if test="${taskFromList.isVisible}">
												Always VISIBLE
											</g:if>
											<g:elseif
													test="${!taskFromList.isVisible && taskFromList.visibleOnTime && taskFromList.date < new Date()}">
												Already VISIBLE since <g:formatDate format="HH:mm, dd MMMM yyyy"
																					date="${taskFromList.date}"/>
											</g:elseif>
											<g:elseif
													test="${!taskFromList.isVisible && taskFromList.visibleOnTime && taskFromList.date > new Date()}">
												INVISIBLE till <g:formatDate format="HH:mm, dd MMMM yyyy"
																			 date="${taskFromList.date}"/>
											</g:elseif>
											<g:else>
												Always INVISIBLE
											</g:else>

									</div>
								</div>
							</div></h3>
						</a>
						<a class="anchorjs-link" href="#-collapsible-list-group-"><span class="anchorjs-icon"></span></a>
					</h4>
				</div>

				<div style="" aria-expanded="true" id="collapseListGroup${i}" class="panel-collapse collapse"
					 role="tabpanel" aria-labelledby="collapseListGroupHeading${i}">
					<ul class="list-group">
						<li class="list-group-item">${raw(taskFromList.description)}</li>
					</ul>
					<div class="panel-footer">
						<g:link class="btn btn-warning" action="editTask"
								id="${courseInstance.id}"
								params="[taskId: taskFromList.id]"><g:message
								code="default.button.edit.label"
								default="Edit"/></g:link>
						<g:link class="btn btn-danger"
								url="[action: 'deleteTask', id: courseInstance.id, params: [taskId: taskFromList.id]]"
								onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
							${message(code: 'default.button.delete.label', default: 'Delete')}
						</g:link>
					</div>
				</div>
			</div>
		</g:else>
	</g:each>
</div>
</body>
</html>
