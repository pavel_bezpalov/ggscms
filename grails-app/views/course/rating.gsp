<%@ page import="ggscms.Rating; ggscms.TestResult; org.springframework.validation.FieldError; ggscms.Color; ggscms.Chapter; ggscms.Course" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'course.label', default: 'Course')}"/>
    <title>Rating</title>
</head>

<body>
<content tag="navbar">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><g:fieldValue bean="${courseInstance}" field="name"/></a>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><g:link action="chapters" id="${courseInstance.id}">Course program</g:link></li>
                    <li><g:link action="lessons" id="${courseInstance.id}">Lessons</g:link></li>
                    <li><g:link action="homeworks" id="${courseInstance.id}">Homeworks</g:link></li>
                    <li><g:link action="tests" id="${courseInstance.id}">Tests <span class="badge"><tests:count/></span></g:link></li>
                    <li class="active"><g:link action="rating" id="${courseInstance.id}">Rating</g:link></li>
                    <membership:teacherOrAdmin>
                        <li><g:link action="users" id="${courseInstance.id}">Users</g:link></li>
                    </membership:teacherOrAdmin>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <g:render template="/common/topbar"/>
                </ul>
            </div><!--/.navbar-collapse -->
        </div>
    </nav>
</content>

<g:if test="${flash.message}">
    <div class="alert alert-info" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        ${flash.message}</div>
</g:if>

    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        <g:each in="${courseInstance?.tests}" status="i" var="testInstance">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="collapseListGroupHeading${i}">
                    <h4 id="-collapsible-list-group-" class="panel-title">
                        <a class="" data-toggle="collapse" href="#collapseListGroup${i}" aria-expanded="true"
                           aria-controls="collapseListGroup${i}">
                            <strong>${fieldValue(bean: testInstance, field: "number")}</strong>
                            ${fieldValue(bean: testInstance, field: "name")}
                        </a>
                        <a class="anchorjs-link" href="#-collapsible-list-group-"><span class="anchorjs-icon"></span>
                        </a></h4>
                </div>

                <div style="" aria-expanded="true" id="collapseListGroup${i}" class="panel-collapse collapse"
                     role="tabpanel" aria-labelledby="collapseListGroupHeading${i}">

                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Full Name</th>
                            <th>Rating %</th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${TestResult.findByTest(testInstance)?.list(sort: 'result', order: 'desc')}"
                                status="y" var="testResultInstance">
                            <tr>
                                <th scope="row">${y + 1}</th>
                                <td>${testResultInstance.user.fullName}</td>
                                <td>${testResultInstance.result}</td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
            </div>
        </g:each>
        <div class="panel panel-danger">
            <div class="panel-heading" role="tab" id="collapseListGroupHeading${i}">
                <h4 id="-collapsible-list-group-" class="panel-title">
                    <a class="" data-toggle="collapse" href="#collapseListGroup${i}" aria-expanded="true"
                       aria-controls="collapseListGroup${i}">
                        <strong>Full Rating</strong>
                    </a>
                    <a class="anchorjs-link" href="#-collapsible-list-group-"><span class="anchorjs-icon"></span></a>
                </h4>
            </div>

            <div style="" aria-expanded="true" id="collapseListGroup${i}" class="panel-collapse collapse in"
                 role="tabpanel" aria-labelledby="collapseListGroupHeading${i}">

                <table class="table table-striped table-condensed">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Full Name</th>
                        <th>Completed Tests</th>
                        <th>Rating %</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${Rating.findByCourse(courseInstance)?.list(sort: 'result', order: 'desc')}"
                            status="y" var="ratingInstance">
                        <tr>
                            <th scope="row">${y + 1}</th>
                            <td>${ratingInstance.user.fullName}</td>
                            <td>${ratingInstance.completedTests}</td>
                            <td>${ratingInstance.result}</td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</body>
</html>

