<html>
<head>
    <meta name="layout" content="main"/>
    <title>INTRUDER SPOTTED!</title>
    <style type="text/css">
    .navbar-brand {
        padding: 5px 5px;
    }
    </style>
    <g:javascript>

        $(document).ready(function() {
            setTimeout(function() {
                window.location.href = "/";
            }, 60000);
        });

    </g:javascript>
</head>
<body>
<g:if test="${flash.message}">
    <div class="alert alert-danger" role="alert">${flash.message}</div>
</g:if>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <h1 class="text-center">INTRUDER SPOTTED!</h1>
        <div class="embed-responsive embed-responsive-16by9">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/lHPEBf6O0yY?autoplay=1" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
</div>

</body>
</html>
