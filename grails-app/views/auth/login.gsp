<html>
<head>
    <meta name="layout" content="main"/>
    <title>Login</title>
    <style type="text/css">
    .navbar-brand {
        padding: 5px 5px;
    }
    </style>
</head>
<body>
<g:if test="${flash.message}">
    <div class="alert alert-danger" role="alert">${flash.message}</div>
</g:if>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <h1 class="text-center">Login</h1>
        <g:form action="signIn">
            <div class="form-group">
                <label for="inputEmail3">Username</label>
                    <input type="text" name="username" value="${username}" class="form-control" id="inputEmail3" placeholder="Username">
            </div>
            <div class="form-group">
                <label for="inputPassword3" >Password</label>
                    <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
            </div>
            <div class="form-group">
                    <div class="checkbox">
			    <input type="hidden" name="_rememberMe">
			    <label>
                            <input type="checkbox" name="rememberMe" id="rememberMe" value="${rememberMe}"> Remember me
                            </label>
                    </div>
            </div>
            <div class="form-group">
                    <button type="submit" class="btn btn-default">Sign in</button>
            </div>
        </g:form>
    </div>
</div>


</body>
</html>
