<shiro:user>
    <g:link class="btn btn-warning navbar-btn navbar-right" controller="auth" action="signOut"><g:message code="topbar.logout"/></g:link>
    <p class="navbar-text navbar-right"><shiro:principal/>&nbsp;&nbsp;&nbsp;</p>
</shiro:user>
<shiro:notUser>
        <g:link class="btn btn-success navbar-btn" controller="auth" action="login">Login</g:link>
        <g:link class="btn btn-info navbar-btn" controller="user" action="register">Register</g:link>
</shiro:notUser>
