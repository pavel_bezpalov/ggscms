<%@ page import="ggscms.User" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <style type="text/css">
    .navbar-brand {
        padding: 5px 5px;
    }
    </style>
</head>

<body>

<g:if test="${flash.message}">
    <div class="alert alert-info" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        ${flash.message}</div>
</g:if>

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>

            <g:sortableColumn property="username" title="${message(code: 'user.username.label', default: 'Username')}"/>

            <g:sortableColumn property="fullName"
                              title="${message(code: 'user.fullName.label', default: 'Full Name')}"/>

            <g:sortableColumn property="email" title="${message(code: 'user.email.label', default: 'Email')}"/>

            <g:sortableColumn property="phone" title="${message(code: 'user.phone.label', default: 'Phone')}"/>

            <g:sortableColumn property="dateCreated"
                              title="${message(code: 'user.dateCreated.label', default: 'Date Created')}"/>

        </tr>
        </thead>
        <tbody>
        <g:each in="${userInstanceList}" status="i" var="userInstance">
            <tr>

                <td><g:link action="show"
                            id="${userInstance.id}">${fieldValue(bean: userInstance, field: "username")}</g:link></td>

                <td>${fieldValue(bean: userInstance, field: "fullName")}</td>

                <td>${fieldValue(bean: userInstance, field: "email")}</td>

                <td>${fieldValue(bean: userInstance, field: "phone")}</td>

                <td><g:formatDate date="${userInstance.dateCreated}"/></td>

            </tr>
        </g:each>
        </tbody>
    </table>
</div>

<div class="pagination">
    <g:paginate total="${userInstanceCount ?: 0}"/>
</div>

<nav class="navbar navbar-inverse navbar-fixed-bottom">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#courses-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="courses-collapse">
            <ul class="nav navbar-nav">
                <g:link class="btn btn-success navbar-btn" controller="course" action="create">New Course</g:link>
            </ul>


            <ul class="nav navbar-nav navbar-right">
                <g:link class="btn btn-primary navbar-btn" controller="user" action="index">All Users</g:link>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>
</nav>

</body>
</html>
