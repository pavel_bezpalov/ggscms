<%@ page import="ggscms.Access; ggscms.Membership" %>
<div class="row">
    <div class="col-xs-2 col-sm-2">
        ${Membership.findByUserAndCourse(user,course) ? Membership.findByUserAndCourse(user,course).access.value : 'none'}
    </div>

    <div class="col-xs-3 col-sm-3 text-right">
        <g:if test="${!Membership.findByUserAndCourse(user,course)}">
            <g:remoteLink class="btn btn-success" action="userAccessChange" resource="${user}"
                          update="${course.id}" params="[courseId: course.id, changeAccess: 'up']">Up</g:remoteLink>
        </g:if>
        <g:elseif test="${Membership.findByUserAndCourse(user,course).access == Access.TEACHER}">
                <g:remoteLink class="btn btn-danger" action="userAccessChange" resource="${user}"
                              update="${course.id}" params="[courseId: course.id, changeAccess: 'down']">Down</g:remoteLink>
        </g:elseif>
        <g:else>
            <g:remoteLink class="btn btn-success" action="userAccessChange" resource="${user}"
                          update="${course.id}" params="[courseId: course.id, changeAccess: 'up']">Up</g:remoteLink>
            <g:remoteLink class="btn btn-danger" action="userAccessChange" resource="${user}"
                          update="${course.id}" params="[courseId: course.id, changeAccess: 'down']">Down</g:remoteLink>
        </g:else>
    </div>
</div>
