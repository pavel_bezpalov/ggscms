<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <title>Registration</title>
    <style type="text/css">
    .navbar-brand {
        padding: 5px 5px;
    }
    </style>
</head>

<body>
<g:if test="${flash.message}">
    <div class="alert alert-danger" role="alert">${flash.message}</div>
</g:if>
<g:hasErrors bean="${user}">
    <ul class="list-group">
        <g:eachError bean="${user}" var="error">
            <li <g:if test="${error in org.springframework.validation.FieldError}">class="list-group-item list-group-item-danger"</g:if>><g:message error="${error}"/></li>
        </g:eachError>
    </ul>
</g:hasErrors>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <h1 class="text-center">Register</h1>
        <g:form action="registration" method="post">
            <div class="form-group ${hasErrors(bean: user, field: 'username', 'errors')}">
                <label class="control-label" for="username">Username:</label>
                <input class="form-control" type="text" name="username" value="${user?.username}"/>
            </div>

            <div class="form-group ${errors?.password}">
            <label for="password">Password:</label>
            <input class="form-control" type="password" name="password" value=""/>
            </div>

            <div class="form-group ${errors?.password}">
                <label for="confirm">Confirm Password:</label>
                <input class="form-control" type="password" name="confirm" value=""/>
            </div>

            <div class="form-group ${hasErrors(bean: user, field: 'fullName', 'errors')}">
            <label for="fullName">Full Name:</label>
            <input class="form-control" type="text" name="fullName" value="${user?.fullName}"/>
            </div>

            <div class="form-group ${hasErrors(bean: user, field: 'email', 'errors')}">
                <label for="email">Email:</label>
                <input class="form-control" type="text" name="email" value="${user?.email}"/>
            </div>

            <div class="form-group ${hasErrors(bean: user, field: 'phone', 'errors')}">
                <label for="phone">Phone:</label>
                <input class="form-control" type="text" name="phone" value="${user?.phone}"/>
            </div>

            <div class="form-group ${errors?.captcha}">
                <label for="code">Enter Code:</label>
                <img src="${createLink(controller: 'simpleCaptcha', action: 'captcha')}"/>
                <g:textField class="form-control" name="captcha"/><br/>
            </div>

            <g:actionSubmit class="btn btn-default" value="Register" action="registration"/>
        </g:form>
    </div>
</div>

</body>
</html>
