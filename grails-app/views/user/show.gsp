<%@ page import="ggscms.Membership; ggscms.Course; ggscms.User" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <style type="text/css">
    .navbar-brand {
        padding: 5px 5px;
    }
    </style>
</head>

<body>

<div class="table-responsive">
    <table class="table table-striped">
        <caption>${userInstance.username} Memberships</caption>
        <thead>
        <tr>

            <th>#</th>
            <th>Course</th>
            <th>Membership</th>

        </tr>
        </thead>
        <tbody>
        <g:each in="${Course.findAll()?.sort() { it.id }}" status="i" var="courseInstance">
            <tr>

                <th scope="row">${i + 1}</th>

                <td>${fieldValue(bean: courseInstance, field: "name")}</td>

                <td>

                    <div class="container-fluid" id="${courseInstance.id}">
                        <g:render template="userAccess" model="[user: userInstance, course: courseInstance]"/>
                    </div>

                </td>

            </tr>
        </g:each>
        </tbody>
    </table>
</div>

<nav class="navbar navbar-inverse navbar-fixed-bottom">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#courses-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="courses-collapse">
            <ul class="nav navbar-nav">
                <g:link class="btn btn-success navbar-btn" controller="course" action="create">New Course</g:link>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <g:link class="btn btn-primary navbar-btn" controller="user" action="index">All Users</g:link>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>
</nav>

</body>
</html>
