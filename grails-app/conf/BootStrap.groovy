import ggscms.*
import org.apache.shiro.crypto.hash.Sha256Hash

class BootStrap {

    def init = { servletContext ->
        environments {
            development {
                //if (!Course.count()) createSampleData()
                // Admin user is required for all environments
                //createAdminUserIfRequired()
            }
            test {
                //if (!Course.count()) createSampleData()
                //createAdminUserIfRequired()
            }
            production {
                if (!Course.count()) createSampleData()
                // Admin user is required for all environments
                createAdminUserIfRequired()
            }
        }

    }

    private createSampleData() {
        println "Creating sample data"
        def adminRole = new Role(name: "administrator")
        adminRole.addToPermissions("*:*")
        adminRole.save(failOnError: true)
        def userRole = new Role(name: "user")
        def user1 = new User(
                username: 'Teacher',
                passwordHash: new Sha256Hash("33333333").toHex(),
                fullName: 'Tea Cher',
                email: 'teacher@geekhub.ck.ua',
                phone: '380000000000')
        user1.save(failOnError: true)
        def user2 = new User(
                username: 'Student',
                passwordHash: new Sha256Hash("22222222").toHex(),
                fullName: 'Stud Ent',
                email: 'student@geekhub.ck.ua',
                phone: '380000000000')
        user2.addToRoles(userRole)
        user2.save(failOnError: true)
        def user3 = new User(
                username: 'Listener',
                passwordHash: new Sha256Hash("11111111").toHex(),
                fullName: 'List Ener',
                email: 'listener@geekhub.ck.ua',
                phone: '380000000000')
        user3.addToRoles(userRole)
        user3.save(failOnError: true)
        def user4 = new User(
                username: 'Subscriber',
                passwordHash: new Sha256Hash("00000000").toHex(),
                fullName: 'Subs Criber',
                email: 'subscriber@geekhub.ck.ua',
                phone: '380000000000')
        user4.addToRoles(userRole)
        user4.save(failOnError: true)
        def frontendCms = new Course(name: "Frontend + CMS",
                description: """Цей курс допоможе вам навчитися створювати веб сайти на основі системи керування контентом. Все, від скінування дизайну до підключення CMS. Цей курс дасть вам чудовий старт для фріланса або роботи в компанії.""",
                shortDescription: """Цей курс допоможе вам навчитися створювати веб сайти на основі системи керування контентом. Все, від скінування дизайну до підключення CMS. Цей курс дасть вам чудовий старт для фріланса або роботи в компанії.""").save(failOnError: true)
        def advancedCms = new Course(name: "Advanced CMS",
                description: """Курс для тих, хто хоче навчитися створювати сайти різного рівня складності за допомогою популярних CMS Wordpress та Drupal. Якщо ви хочете робити вебсайти як професіонал, цей курс для вас. Особливості різних CMS, використання та написання плагінів та компонентів, інтеграція із соціальними мережами та сторонніми сервісами.""",
                shortDescription: """Курс для тих, хто хоче навчитися створювати сайти різного рівня складності за допомогою популярних CMS Wordpress та Drupal. Якщо ви хочете робити вебсайти як професіонал, цей курс для вас. Особливості різних CMS, використання та написання плагінів та компонентів, інтеграція із соціальними мережами та сторонніми сервісами.""").save(failOnError: true)
        def advancedPhp = new Course(name: "Advanced PHP",
                description: """Для тих хто хоче розвивати свої базові навички в PHP. Курс заглиблюється у вивчення ООП, MVC та паттернiв проетування на базі найпопулярнішого php фреймворку Symfony2.""",
                shortDescription: """Для тих хто хоче розвивати свої базові навички в PHP. Курс заглиблюється у вивчення ООП, MVC та паттернiв проетування на базі найпопулярнішого php фреймворку Symfony2.""").save(failOnError: true)
        def javaScript = new Course(name: "JavaScript",
                description: """Все найцікавіше відбувається в браузері, а не на сервері. Javascript - це той інструмент який допоможе вам створювати дійсно зручні та швидкі веб сторінки.""",
                shortDescription: """Все найцікавіше відбувається в браузері, а не на сервері. Javascript - це той інструмент який допоможе вам створювати дійсно зручні та швидкі веб сторінки.""").save(failOnError: true)
        def iOs = new Course(name: "iOS",
                description: """Курс для швидкого старту в розробці програмного забезпечення під популярну мобільну платформу iOS. """,
                shortDescription: """Курс для швидкого старту в розробці програмного забезпечення під популярну мобільну платформу iOS. """).save(failOnError: true)
        def groovyGrails = new Course(name: "Groovy & Grails",
                description: """<p>Познайомтесь з одним з найпотужніших фреймворків для швидкої розробки веб-додатків. Гнучкість динамічної мови Groovy (що є надмножиною Java, що на 100% сумiсна Groovy) поєднуються з силою та рiзноманiтнiстю Java бiблiотек та фреймворкiв. Grails - це не тільки набір бібліотек, але й певна філософія розробки, що базуеться на паттернах та найкращих практиках, які стали загальноприйнятими не тільки в середовищі Java, але й в програмуванні вцілому. Спираючись на відносно низький поріг входження, ми спробуемо розглянути повний цикл розробки веб-додатку: побудова інтерфейсу, обробка внутрішньої логіки, зберігання даних.</p>

<h3>Команда</h3>
                    <ul>
                        <li>
                            <img src="images/bogdan_danilyuk.png" />
                            <h3>Богдан Данилюк</h3>
                            <p>Богдан один з головних розробників компанії TransferWise. Прихильник швидких, динамічних та надійних рішень у розробці програмного забезпечення. Викладатиме курс улюбленої мови програмування та фреймворку - Groovy & Grails.</p>
                        </li>
                        <li>
                            <img src="images/vadym_vasiliev.png" />
                            <h3>Вадим Васильєв</h3>
                            <p>Вадим розробник у компанії TransferWise. У попередньому працював розробником та консультантом у банках різних країн Європи та світу. У GeekHub викладає Groovy & Grails.</p>
                        </li>
                        <li>
                            <img src="images/sergiy_karpenko.png" />
                            <h3>Сергій Карпенко</h3>
                            <p>Сергій один з перших розробників у компанії TransferWise. У GeekHub викладає Groovy & Grails.</p>
                        </li>
                    </ul>

<h3>ТЕМИ ПИТАНЬ НА ЕКЗАМЕНI</h3>

<ul>
\t<li>Знання структур данних та основ програмування (цикли, розгалудження і т.д.)</li>
\t<li>Базові знання HTML/CSS</li>
\t<li>Базові знання ООП</li>
\t<li>Базовий рівень англійської в IT</li>
\t<li>Загальне розуміння роботи web (протоколи, http статуси/заголовки/кукі, структура URL)</li>
\t<li>Можливі питання базового рівню по Java, Unix командам та теорії баз данних.&quot;</li>
</ul>

<h3>Слово від викладачів</h3>

<p>Команда викладачів курсу Groovy &amp; Grails хотіла б привернути увагу людей з досвідом програмування (бажано java-stack), що хочуть вивчити новий та потужний інструмент для побудови свого проекту/стартапу. У той же час, керуючись досвідом минулого року, можна сказати, що низький поріг входження та краса :) Groovy &amp; Grails дозволяють успішно завершити цей курс наполегливим студентам, які ще не мають бойового досвіду у програмуванні. Програма курсу дає можливість поступово освоїти Groovy та Grails. Починаючи з основи синтаксису і shell-скриптів на Groovy, і закінчуючи побудовою модульного та добре налаштованого Grails проекту. Під час курсу ми також розглянемо найкращі практики програмування та шаблони проектування (design patterns).</p>

<p>Для проходження курсу рекомендовано:<br />
- ознайомитися з основами Java та особливостями роботи JVM<br />
- мати ноутбук, бажано з UNIX системою та вмінням управлятися з нею (Linux Mint, Ubuntu, Mac OS X і т.д.)<br />
- велике бажання та мотивація :)&quot;</p>
""",
                shortDescription: """Познайомтесь з одним з найпотужніших фреймворків для швидкої розробки веб-додатків.""").save(failOnError: true)
        new Membership(access: Access.TEACHER, course: groovyGrails, user: user1).save(failOnError: true)
        new Membership(access: Access.STUDENT, course: groovyGrails, user: user2).save(failOnError: true)
        new Membership(access: Access.LISTENER, course: groovyGrails, user: user3).save(failOnError: true)
        new Membership(access: Access.SUBSCRIBED, course: groovyGrails, user: user4).save(failOnError: true)
        def ggChapter1 = new Chapter(number: 1, course: groovyGrails, name: "Intro",
                description: """
<p>- Course structure</p>
<p>- Why Goovy &amp; Grails</p>
<p>- Check knowledge level of students: OOP, VCS, Patterns, Web application basics</p>
""", date: new Date(115, 8, 1, 00, 00)).save(failOnError: true)
        def ggChapter2 = new Chapter(number: 2, course: groovyGrails, name: "The Groovy Language: Introduction",
                description: """
<p>- Introduction to The Groovy language</p>
<p>- Installation and Tools (Git, IDE, Java, Groovy, GVM)</p>
<p>- Differences to Other Languages</p>
<p>- Writing and Executing Groovy Scripts</p>
""", date: new Date(115, 8, 8, 18, 30)).save(failOnError: true)
        def ggChapter3 = new Chapter(number: 3, course: groovyGrails, name: "The Groovy Language: Language Basics and Data Types",
                description: """
<p>- Everything is an object</p>

<p>- Numbers</p>

<p>- Optional Typing, def</p>

<p>- Standard Operators, Operator Overriding</p>

<p>- Java Strings and Groovy Strings</p>

<p>- Methods in Groovy</p>

<p>- Groovy Truth</p>

<p>- Assertions in Groovy</p>

<p>- Regular Expressions</p>
""", date: new Date(115, 8, 15, 18, 30)).save(failOnError: true)
        def lesson1 = new Task(isVisible: true, name: "Урок 1", description: "<p>Тема: Intro.</p><p>Место: Дом Евангелия 2 этаж большой зал</p>", type: Type.LESSON, course: groovyGrails).save(failOnError: true)
        def homework1 = new Task(isVisible: false, date: new Date(115, 8, 1, 20, 30), name: "Домашка 1", description: """
<p>1) Установить и настроить Groovy это можно сделать проще всего используя gvm tool или следуя инструкциям описаным тут.</p>

<p>2) Разобраться с git, почитать тут или на любом из посвящённых ресурсов</p>

<p>3) Создать свой репозиторий на <a href="https://bitbucket.org/">Bitbucket</a></p>

<p>4) Написать несколько тестов на запомнившиеся фичи груви. <a href="https://bitbucket.org/Dang_GG/geekhub/commits/27bf95df313951984a7810f9d410353ea62225bf">Вот к примеру, простой тест на &quot;?:&quot; Elvis оператор .Тест запускается из командной строки или через groovyConsole (редактор, входит в groovy пакет). </a></p>

<p>5) Кто хочет посложнее, тесты можно написать с использованием JUnit&#39;a или Spock&#39;a. <a href="https://www.jetbrains.com/idea/download/">Рекомендуемый IDE</a>&nbsp;</p>

<p>6) <a href="https://docs.google.com/spreadsheet/ccc?key=0Ag3Pclj03VG2dERLcHFNUUVabXdOeFBXVVliNGFZcXc#gid=0">Ccылку на репозиторий указать сюда (Repos вкладка)</a></p>

<p>7) Добавить vadimv.dev в скайпе, я создам группу студенты + преподаватели. Вопросы по заданию можно задавать как и в письмах так и в скайпе.</p>
""", type: Type.HOMEWORK, course: groovyGrails).save(failOnError: true)
    }

    private createAdminUserIfRequired() {
        println "Creating admin user"
        if (!User.findByUsername("admin")) {
            println "Fresh Database. Creating ADMIN user."
            def user = new User(username: "admin",
                    passwordHash: new Sha256Hash("ktPncqS5").toHex(),
                    fullName: "Administrator",
                    email: "admin@mail.com",
                    phone: '380000000000')
            def adminRole = Role.findByName("administrator")
            if (!adminRole) {
                adminRole = new Role(name: "administrator")
                adminRole.addToPermissions("*:*")
                adminRole.save(flush: true)
		def userRole = new Role(name: "user")
		userRole.save(flush: true)
            }
            user.addToRoles(adminRole)
            user.save(flush: true)
        } else {
            println "Existing admin user, skipping creation"
        }
    }
    def destroy = {
    }
}