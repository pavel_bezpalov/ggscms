package ggscms

import grails.transaction.Transactional
import org.apache.shiro.crypto.hash.Sha256Hash

@Transactional(readOnly = true)
class UserController {

    def simpleCaptchaService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond User.list(params), model: [userInstanceCount: User.count()]
    }

    def show(User userInstance) {
        respond userInstance
    }

    @Transactional
    def userAccessChange(User userInstance) {
        Course courseInstance = Course.findById(params.courseId)
        Membership membership = Membership.findByUserAndCourse(userInstance, courseInstance)

        if (!membership && params.changeAccess == "up") {
            membership = new Membership(course: courseInstance, user: userInstance, access: Access.SUBSCRIBED)
            membership.save(flush: true)
            render(template: "/user/userAccess", model: [course: courseInstance, user: userInstance])
            return
        }

        if (params.changeAccess == "up") {
            switch (membership.access) {
                case Access.SUBSCRIBED: membership.access = Access.LISTENER
                    break
                case Access.LISTENER: membership.access = Access.STUDENT
                    break
                case Access.STUDENT: membership.access = Access.TEACHER
                    break
            }
            membership.save(flush: true)
            render(template: "/user/userAccess", model: [course: courseInstance, user: userInstance])
        } else if (params.changeAccess == "down") {
            switch (membership.access) {
                case Access.TEACHER: membership.access = Access.STUDENT
                    membership.save(flush: true)
                    break
                case Access.STUDENT: membership.access = Access.LISTENER
                    membership.save(flush: true)
                    break
                case Access.LISTENER: membership.access = Access.SUBSCRIBED
                    membership.save(flush: true)
                    break
                case Access.SUBSCRIBED: membership.delete(flush: true)
                    break
            }
            render(template: "/user/userAccess", model: [course: courseInstance, user: userInstance])
        }
    }

    def register() {}

    @Transactional
    def registration() {
        def user = new User(username: params.username,
                passwordHash: new Sha256Hash(params.password).toHex(),
                fullName: params.fullName,
                email: params.email,
                phone: params.phone)
        def userRole = Role.findByName("user")
        user.addToRoles(userRole)

        boolean captchaValid = simpleCaptchaService.validateCaptcha(params.captcha)

        def errors = [:]

        if (captchaValid) {
            if (params.password != params.confirm) {
                flash.message = "The two passwords you entered dont match!"
                errors = [password: "error"]
                render(view: 'register', model: [user: user, errors: errors])
            } else if (!(params.password.size() in 8..16)) {
                flash.message = "Password must contain 8..16 characters!"
                errors = [password: "error"]
                render(view: 'register', model: [user: user, errors: errors])
            } else {
                if (user.save()) {
                    redirect(controller: "auth", action: "signIn", params: [username: params.username, password: params.password])
                } else {
                    flash.user = user
                    render(view: 'register', model: [user: user, errors: errors])
                }
            }
        } else {
            flash.message = "Access code did not match."
            errors = [captcha: "error"]
            render(view: 'register', model: [user: user, errors: errors])
        }
    }
}