package ggscms

import grails.transaction.Transactional
import org.apache.shiro.SecurityUtils

@Transactional(readOnly = true)
class CourseController {

    def courses(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Course.list(params), model: [courseInstanceCount: Course.count()]
    }

    def create() {
        respond new Course(params)
    }

    @Transactional
    def save(Course courseInstance) {
        if (courseInstance == null) {
            notFound()
            return
        }

        if (courseInstance.hasErrors()) {
            respond courseInstance.errors, view: 'create'
            return
        }

        courseInstance.save flush: true


        flash.message = message(code: 'default.created.message', args: [message(code: 'course.label', default: 'Course'), courseInstance.name])
        redirect action: "courses"
    }

    def edit(Course courseInstance) {
        respond courseInstance
    }

    @Transactional
    def update(Course courseInstance) {
        if (courseInstance == null) {
            notFound()
            return
        }

        if (courseInstance.hasErrors()) {
            respond courseInstance.errors, view: 'edit'
            return
        }

        courseInstance.save flush: true

        flash.message = message(code: 'default.updated.message', args: [message(code: 'Course.label', default: 'Course'), courseInstance.name])
        redirect action: "courses"
    }

    @Transactional
    def delete(Course courseInstance) {

        if (courseInstance == null) {
            notFound()
            return
        }

        Membership.findAllByCourse(courseInstance).each {
            it.delete flush: true
        }

        courseInstance.delete flush: true

        flash.message = message(code: 'default.deleted.message', args: [message(code: 'Course.label', default: 'Course'), courseInstance.name])
        redirect action: "courses"
    }

    protected void notFound() {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'course.label', default: 'Course'), params.id])
        redirect action: "courses"
    }

    def users(Course courseInstance) {
        def users = User.where {
            memberships.course == courseInstance
        }.list().each {
            it.membership = Membership.findByUserAndCourse(it, courseInstance).access
        }

        if (params.order == 'asc') {
            users.sort { it.getProperty(params.sort) }
        } else if (params.order == 'desc') {
            users.sort { a, b ->
                b.getProperty(params.sort) <=> a.getProperty(params.sort)
            }
        }

        respond courseInstance, model: [users: users, course: courseInstance]
    }

    @Transactional
    def userAccessChange(Long id) {
        String userName = SecurityUtils.subject?.principal
        Course courseInstance = Course.findById(id)
        User manager = User.findByUsername("$userName")
        Membership managerMembership = Membership.findByUserAndCourse(manager, courseInstance)

        User user = User.findById(params.userId)
        Membership membership = Membership.findByUserAndCourse(user, courseInstance)

        if (params.changeAccess == "up") {
            switch (membership.access) {
                case Access.SUBSCRIBED: membership.access = Access.LISTENER
                    break
                case Access.LISTENER: membership.access = Access.STUDENT
                    break
                case Access.STUDENT:
                    if (managerMembership?.access == Access.TEACHER) {
                        break
                    }
                    membership.access = Access.TEACHER
                    break
            }
            membership.save(flush: true)
            user.membership = membership.access
            render(template: "/course/userAccess", model: [course: courseInstance, user: user])
        } else if (params.changeAccess == "down") {
            switch (membership.access) {
                case Access.TEACHER:
                    if (managerMembership?.access == Access.TEACHER) {
                        break
                    }
                    membership.access = Access.STUDENT
                    break
                case Access.STUDENT: membership.access = Access.LISTENER
                    break
                case Access.LISTENER: membership.access = Access.SUBSCRIBED
                    break
            }
            membership.save(flush: true)
            user.membership = membership.access
            render(template: "/course/userAccess", model: [course: courseInstance, user: user])
        }
    }

    @Transactional
    def subscribe(Course courseInstance) {
        String userName = SecurityUtils.subject?.principal
        User user = User.findByUsername("$userName")
        Membership membership = Membership.findByUserAndCourse(user, courseInstance)
        if (!membership) {
            new Membership(user: user, course: courseInstance, access: Access.SUBSCRIBED).save(flush: true)
            render(template: "/course/access", model: [course: courseInstance])
        } else if (membership.access == Access.SUBSCRIBED) {
            membership.delete(flush: true)
            render(template: "/course/access", model: [course: courseInstance])
        }
    }

    def tests(Course courseInstance) {
        String userName = SecurityUtils.subject?.principal
        User user = User.findByUsername("$userName")
        Membership membership = Membership.findByUserAndCourse(user, courseInstance)
        Test newTest = new Test(params)
        def tests = []

        if (membership?.access == Access.TEACHER || Role.findByName('administrator') in user.roles) {
            tests = Test.where {
                course == courseInstance
            }.list(params)
        } else tests = Test.where {
            course == courseInstance
            (isVisible == true) || (visibleOnTime == true && date < new Date())
        }.list(sort: 'number', order: 'asc')

        respond courseInstance, model: [tests: tests, newTest: newTest]
    }

    @Transactional
    def saveTest(Long id) {
        Course courseInstance = Course.findById(id)
        Test testInstance = new Test(params)

        def tests = Test.where {
            course == courseInstance
        }.list(params)

        if (!testInstance.validate()) {
            render(view: 'tests', model: [courseInstance: courseInstance, newTest: testInstance, tests: tests])
            return
        }

        testInstance.save flush: true

        flash.message = message(code: 'default.created.message', args: [message(code: 'test.label', default: 'Test'), testInstance.name])
        redirect action: 'addQuestionsToTest', id: id, params: [testId: testInstance.id]
    }

    def editTest(Long id) {
        Course courseInstance = Course.findById(id)
        Test testInstance = Test.findById(params.testId)

        def tests = Test.where {
            course == courseInstance
        }.list(params)

        render(view: 'test/editTest', model: [tests: tests, testInstance: testInstance, courseInstance: courseInstance])
    }

    @Transactional
    def updateTest(Long id) {
        Course courseInstance = Course.findById(id)
        Test testInstance = Test.findById(params.testId)

        def tests = Test.where {
            course == courseInstance
        }.list(params)

        testInstance.properties = params

        if (!testInstance.validate()) {
            render(view: 'test/editTest', model: [tests: tests, courseInstance: courseInstance, testInstance: testInstance])
            return
        }

        testInstance.save flush: true

        flash.message = message(code: 'default.updated.message', args: [message(code: 'Test.label', default: 'Test'), testInstance.name])
        redirect action: 'tests', id: id
    }

    @Transactional
    def deleteTest(Long id) {
        Test testInstance = Test.findById(params.testId)

        testInstance.delete flush: true

        flash.message = message(code: 'default.deleted.message', args: [message(code: 'Test.label', default: 'Test'), testInstance.name])
        redirect action: 'tests', id: id
    }

	@Transactional
    def testing(Long id) {
        Course courseInstance = Course.findById(id)
        Test testInstance = Test.findById(params.testId)
        String userName = SecurityUtils.subject?.principal
        User user = User.findByUsername("$userName")
        TestResult testResult = TestResult.findByUserAndTest(user, testInstance)
        def timeLimit = testInstance.timeLimit * 60 * 1000
        def timeLeft = testInstance.timeLimit * 60
        def currentTime = new Date().time

        if (testInstance.questions.size() == 0) {
            flash.message = message(code: 'default.testCompleted.message', args: [testInstance.name], default: "Test has not any questions.")
            redirect action: 'tests', id: id
            return
        }

        if (!testResult) {
            testResult = new TestResult(user: user, test: testInstance)

            testResult.save(flush: true)

            render view: "test/testing", model: [courseInstance: courseInstance, testInstance: testInstance, timeLeft: timeLeft, user: user]
        } else {
            def startTime = testResult.dateCreated.time

            def offTime = startTime + timeLimit

            if (offTime > currentTime) {
                timeLeft = (timeLimit - (currentTime - startTime)) / 1000
                render view: "test/testing", model: [courseInstance: courseInstance, testInstance: testInstance, timeLeft: timeLeft, user: user]
            } else {
                flash.message = message(code: 'default.testCompleted.message', args: [testInstance.name], default: "You have already completed {0} test")
                redirect action: 'tests', id: id
            }
        }
    }

    @Transactional
    def submitTest(Long id) {
        Test testInstance = Test.findById(params.testId)
        String userName = SecurityUtils.subject?.principal
        User currentUser = User.findByUsername("$userName")
        TestResult testResult = TestResult.findByUserAndTest(currentUser, testInstance)
        Course courseInstance = Course.findById(id)
        def timeLimit = testInstance.timeLimit * 60 * 1000
        def currentTime = new Date().time
        def startTime = testResult?.dateCreated?.time
        def offTime = startTime + timeLimit + 60000
        def countQuestions = testInstance.questions.size()

        if (offTime > currentTime) {

            for (int i in 1..countQuestions) {

                if (params["questionId$i"]) {
                    Question question = Question.findById(params["questionId$i"])

                    Answer findSubmittedAnswer = Answer.findByUserAndTestAndQuestion(currentUser, testInstance, question)
                    if (findSubmittedAnswer) {
                        findSubmittedAnswer.delete(flush: true)
                    }

                    Answer answer = new Answer(user: currentUser, test: testInstance, question: question)

                    for (int y in 1..5) {
                        if (params["variantId$i$y"] && params["variantAnswer$i$y"]) {
                            Variant variant = Variant.findById(params["variantId$i$y"])
                            answer.addToVariants(variant)
                        }
                    }
                    answer.save(flush: true)
                }
            }

            // Calculating result

            float result = 0

            for (Question questionInstance in testInstance.questions) {
                float questionRating = 0
                Answer findUserAnswer = Answer.findByUserAndQuestion(currentUser,questionInstance)
                if (findUserAnswer) {
                    for (Variant variantInstance in questionInstance.variants) {
                        if ( (variantInstance in findUserAnswer.variants && variantInstance.correct) || (!(variantInstance in findUserAnswer.variants) && !(variantInstance.correct))) {
                            questionRating = questionRating + 1
                        }
                    }
                    questionRating = questionRating / questionInstance.variants.size()
                }
                result = result + questionRating
            }

            result = result / testInstance.questions.size()

            testResult.result = (result * 100).toInteger()

            testResult.save()

            // Calculation Rating

            def testResultsByCourse = TestResult.where {
                user == currentUser
                test in Test.findByCourse(courseInstance)
            }.list()

            Integer fullResult = 0
            Integer completedTests = testResultsByCourse.size()

            for (TestResult testResultForCountRating in testResultsByCourse) {
                fullResult = fullResult + testResultForCountRating.result
            }

            fullResult = fullResult / completedTests

            Rating rating = Rating.findByCourseAndUser(courseInstance,currentUser)

            if (!rating) {
                rating = new Rating(course: courseInstance, user: currentUser, completedTests: completedTests, result: fullResult).save(flush: true)
            } else {
                rating.completedTests = completedTests
                rating.result = fullResult
                rating.save(flush: true)
            }

            flash.message = message(code: 'default.submitTest.message', args: [message(code: 'Test.label', default: 'Test'), testInstance.name], default: "{0} {1} submitted")
            redirect action: 'tests', id: id
        } else {
            flash.message = message(code: 'default.testCompleted.message', args: [testInstance.name], default: "You've trying to hack with no luck! :)")
            redirect action: 'tests', id: id
        }
    }

    def viewTestResult(Long id) {
        Course courseInstance = Course.findById(id)
        Test testInstance = Test.findById(params.testId)
        String userName = SecurityUtils.subject?.principal
        User user = User.findByUsername("$userName")
        Membership membership = Membership.findByUserAndCourse(user, courseInstance)
        TestResult testResultInstance = TestResult.findByUserAndTest(user, testInstance)
        def timeLimit = testInstance.timeLimit * 60 * 1000
        def currentTime = new Date().time

        if (testResultInstance) {
            def startTime = testResultInstance.dateCreated.time

            def offTime = startTime + timeLimit

            def userAccess = offTime < currentTime && testInstance.allowViewResults
            def teacherOrAdminAccess = membership?.access == Access.TEACHER || Role.findByName('administrator') in user.roles

            if (userAccess || teacherOrAdminAccess ) {
                render view: "test/viewTestResult", model: [courseInstance: courseInstance, testInstance: testInstance, user: user]
            } else {
                flash.message = message(code: 'default.testCompleted.message', args: [testInstance.name], default: "View test results of {0} is not permitted.")
                redirect action: 'tests', id: id
            }
        } else {
            flash.message = message(code: 'default.testCompleted.message', args: [testInstance.name], default: "You must complete {0} first to view its result.")
            redirect action: 'tests', id: id
        }
    }

    def rating(Course courseInstance) {
        respond courseInstance
    }

    def addQuestionsToTest(Long id) {
        Course courseInstance = Course.findById(id)
        Test testInstance = Test.findById(params.testId)
        Question newQuestion = new Question(params)
        render view: 'test/addQuestionsToTest', model: [courseInstance: courseInstance, testInstance: testInstance, newQuestion: newQuestion]
    }

    @Transactional
    def saveQuestion(Long id) {
        Course courseInstance = Course.findById(id)
        Test testInstance = Test.findById(params.test.id)
        Question newQuestion = new Question(params)

        if (!newQuestion.validate()) {
            render(view: 'test/addQuestionsToTest', model: [courseInstance: courseInstance, testInstance: testInstance, newQuestion: newQuestion])
            return
        }

        newQuestion.save flush: true

        for (int i in 1..5) {

            if (params["variantName$i"]) {
                Variant variant = new Variant(name: params["variantName$i"])
                if (params["variantCorrect$i"]) {
                    variant.correct = true
                }
                newQuestion.addToVariants(variant)
                newQuestion.save flush: true
            }
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'test.label', default: 'Question'), newQuestion.name])
        redirect action: 'addQuestionsToTest', id: id, params: [testId: testInstance.id]
    }

    def editQuestion(Long id) {
        Course courseInstance = Course.findById(id)
        Question questionInstance = Question.findById(params.questionId)
        render(view: 'test/editQuestion', model: [questionInstance: questionInstance, courseInstance: courseInstance])
    }

    @Transactional
    def updateQuestion(Long id) {
        Course courseInstance = Course.findById(id)
        Question questionInstance = Question.findById(params.questionId)

        questionInstance.properties = params

        if (!questionInstance.validate()) {
            render(view: 'test/editQuestion', model: [questionInstance: questionInstance, courseInstance: courseInstance])
            return
        }

        questionInstance.save flush: true

        for (int i in 1..5) {

            if (params["variantId$i"]) {
                Variant variant = Variant.findById(params["variantId$i"])
                if (params["variantName$i"]) {

                    variant.name = params["variantName$i"]
                    if (params["variantCorrect$i"]) {
                        variant.correct = true
                    }
                    variant.save flush: true
                } else {
                    variant.delete flush: true
                }
            }

            if (params["variantName$i"] && params["variantId$i"] == null) {
                Variant variant = new Variant(name: params["variantName$i"])
                if (params["variantCorrect$i"]) {
                    variant.correct = true
                }
                questionInstance.addToVariants(variant)
                questionInstance.save flush: true
            }
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'Test.label', default: 'Question'), questionInstance.name])
        redirect action: 'addQuestionsToTest', id: id, params: [testId: questionInstance.test.id]
    }

    @Transactional
    def deleteQuestion(Long id) {
        Question question = Question.findById(params.questionId)

        question.delete flush: true

        flash.message = message(code: 'default.deleted.message', args: [message(code: 'Test.label', default: 'Question'), question.name])
        redirect action: 'addQuestionsToTest', id: id, params: [testId: question.test.id]
    }

    def chapters(Long id) {
        Chapter newChapter = new Chapter(params)
        Course courseInstance = Course.findById(id)

        def chapters = Chapter.where {
            course == courseInstance
        }.list(sort: 'number', order: 'asc')

        respond courseInstance, model: [chapters: chapters, newChapter: newChapter]
    }

    @Transactional
    def saveChapter(Long id) {
        Course courseInstance = Course.findById(id)
        Chapter chapterInstance = new Chapter(params)

        def chapters = Chapter.where {
            course == courseInstance
        }.list(sort: 'number', order: 'asc')

        if (!chapterInstance.validate()) {
            render(view: 'chapters', model: [courseInstance: courseInstance, newChapter: chapterInstance, chapters: chapters])
            return
        }

        chapterInstance.save flush: true

        flash.message = message(code: 'default.created.message', args: [message(code: 'test.label', default: 'Chapter'), chapterInstance.name])
        redirect action: 'chapters', id: id
    }

    def editChapter(Long id) {
        Course courseInstance = Course.findById(id)
        Chapter chapterInstance = Chapter.findById(params.chapterId)

        def chapters = Chapter.where {
            course == courseInstance
        }.list(sort: 'number', order: 'asc')

        render(view: 'chapter/editChapter', model: [chapters: chapters, chapter: chapterInstance, courseInstance: courseInstance])
    }

    @Transactional
    def updateChapter(Long id) {
        Course courseInstance = Course.findById(id)
        Chapter chapterInstance = Chapter.findById(params.chapterId)

        chapterInstance.properties = params

        def chapters = Chapter.where {
            course == courseInstance
        }.list(sort: 'number', order: 'asc')

        if (!chapterInstance.validate()) {
            render(view: 'chapter/editChapter', model: [chapters: chapters, courseInstance: courseInstance, chapter: chapterInstance])
            return
        }

        chapterInstance.save flush: true

        flash.message = message(code: 'default.updated.message', args: [message(code: 'Test.label', default: 'Chapter'), chapterInstance.name])
        redirect action: 'chapters', id: id
    }

    @Transactional
    def deleteChapter(Long id) {
        Chapter chapterInstance = Chapter.findById(params.chapterId)

        chapterInstance.delete flush: true

        flash.message = message(code: 'default.deleted.message', args: [message(code: 'Test.label', default: 'Chapter'), chapterInstance.name])
        redirect action: 'chapters', id: id
    }

    def lessons(Long id) {
        String userName = SecurityUtils.subject?.principal
        User user = User.findByUsername("$userName")
        Task newTask = new Task(params)
        Course courseInstance = Course.findById(id)
        Membership membership = Membership.findByUserAndCourse(user, courseInstance)

        def lessons = []

        if (membership?.access == Access.TEACHER || Role.findByName('administrator') in user.roles) {
            lessons = Task.where {
                course == courseInstance
                type == Type.LESSON
            }.list(sort: 'number', order: 'asc')
        } else lessons = Task.where {
            course == courseInstance
            type == Type.LESSON
            (isVisible == true) || (visibleOnTime == true && date < new Date())
        }.list(sort: 'number', order: 'asc')

        respond courseInstance, model: [lessons: lessons, newTask: newTask]
    }

    def homeworks(Long id) {
        String userName = SecurityUtils.subject?.principal
        User user = User.findByUsername("$userName")
        Task newTask = new Task(params)
        Course courseInstance = Course.findById(id)
        Membership membership = Membership.findByUserAndCourse(user, courseInstance)

        def homeworks = []

        if (membership?.access == Access.TEACHER || Role.findByName('administrator') in user.roles) {
            homeworks = Task.where {
                course == courseInstance
                type == Type.HOMEWORK
            }.list(sort: 'number', order: 'asc')
        } else homeworks = Task.where {
            course == courseInstance
            type == Type.HOMEWORK
            (isVisible == true) || (visibleOnTime == true && date < new Date())
        }.list(sort: 'number', order: 'asc')

        respond courseInstance, model: [homeworks: homeworks, newTask: newTask]
    }

    @Transactional
    def saveTask(Long id) {
        Course courseInstance = Course.findById(id)
        Task taskInstance = new Task(params)

        def lessons = Task.where {
            course == courseInstance
            type == Type.LESSON
        }.list(sort: 'number', order: 'asc')

        def homeworks = Task.where {
            course == courseInstance
            type == Type.HOMEWORK
        }.list(sort: 'number', order: 'asc')

        if (!taskInstance.validate()) {
            if (params.type == "LESSON") {
                render(view: 'lessons', model: [lessons: lessons, courseInstance: courseInstance, newTask: taskInstance])
                return
            } else if (params.type == "HOMEWORK") {
                render(view: 'homeworks', model: [homeworks: homeworks, courseInstance: courseInstance, newTask: taskInstance])
                return
            }
        }

        taskInstance.save flush: true

        flash.message = message(code: 'default.created.message', args: [message(code: 'test.label', default: 'Task'), taskInstance.name])
        if (params.type == "LESSON") {
            redirect action: 'lessons', id: id
        } else if (params.type == "HOMEWORK") {
            redirect action: 'homeworks', id: id
        }
    }

    def editTask(Long id) {
        Course courseInstance = Course.findById(id)
        Task taskInstance = Task.findById(params.taskId)

        def lessons = Task.where {
            course == courseInstance
            type == Type.LESSON
        }.list(sort: 'number', order: 'asc')

        def homeworks = Task.where {
            course == courseInstance
            type == Type.HOMEWORK
        }.list(sort: 'number', order: 'asc')

        if (taskInstance.type == Type.LESSON) {
            render(view: 'task/editTask', model: [tasks: lessons, taskInstance: taskInstance, courseInstance: courseInstance])
        } else {
            render(view: 'task/editTask', model: [tasks: homeworks, taskInstance: taskInstance, courseInstance: courseInstance])
        }
    }

    @Transactional
    def updateTask(Long id) {
        Course courseInstance = Course.findById(id)
        Task taskInstance = Task.findById(params.taskId)

        taskInstance.properties = params

        def lessons = Task.where {
            course == courseInstance
            type == Type.LESSON
        }.list(sort: 'number', order: 'asc')

        def homeworks = Task.where {
            course == courseInstance
            type == Type.HOMEWORK
        }.list(sort: 'number', order: 'asc')

        if (!taskInstance.validate()) {
            if (taskInstance.type == Type.LESSON) {
                render(view: 'task/editTask', model: [tasks: lessons, taskInstance: taskInstance, courseInstance: courseInstance])
                return
            } else {
                render(view: 'task/editTask', model: [tasks: homeworks, taskInstance: taskInstance, courseInstance: courseInstance])
                return
            }
        }

        taskInstance.save flush: true

        flash.message = message(code: 'default.updated.message', args: [message(code: 'Test.label', default: 'Task'), taskInstance.name])
        if (taskInstance.type.name() == "LESSON") {
            redirect action: 'lessons', id: id
        } else if (taskInstance.type.name() == "HOMEWORK") {
            redirect action: 'homeworks', id: id
        }
    }

    @Transactional
    def deleteTask(Long id) {
        Task taskInstance = Task.findById(params.taskId)

        def type = taskInstance.type.name()

        taskInstance.delete flush: true

        flash.message = message(code: 'default.deleted.message', args: [message(code: 'Test.label', default: 'Task'), taskInstance.name])
        if (type == "LESSON") {
            redirect action: 'lessons', id: id
        } else if (type == "HOMEWORK") {
            redirect action: 'homeworks', id: id
        }
    }
}