
package ggscms

import grails.test.mixin.Mock
import org.apache.shiro.crypto.hash.Sha256Hash
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@Mock([Course, User, Membership, Role])
class UserSpec extends Specification {

    def "Saving our first user to the database"() {
        given: "A brand new user"
        def pavel = new User(username: 'pavel',
                passwordHash:  new Sha256Hash("55555555").toHex(),
                fullName: 'Pavel Bezpalov',
                email: 'pavel_bezpalov@geekhub.ck.ua',
                phone: '380000000000')
        when: "the user is saved"
        pavel.save()
        then: "it saved successfully and can be found in the database"
        pavel.errors.errorCount == 0
        pavel.id != null
        User.get(pavel.id).fullName == pavel.fullName != null
        User.get(pavel.id).username == pavel.username
    }

    def "Updating a saved user changes its properties"() {
        given: "An existing user"
        def existingUser = new User(username: 'pavel',
                passwordHash:  new Sha256Hash("55555555").toHex(),
                fullName: 'Parampampam',
                email: 'pavel_bezpalov@geekhub.ck.ua',
                phone: '380000000000').save(failOnError: true)
        when: "A property is changed"
        def foundUser = User.get(existingUser.id)
        foundUser.fullName = 'Bezpalov Pavel'
        foundUser.save(failOnError: true)
        then: "The change is reflected in the database"
        User.get(existingUser.id).fullName == 'Bezpalov Pavel'
    }

    def "Deleting an existing user removes it from the database"() {
        given: "An existing user"
        def user = new User(username: 'pavel',
                passwordHash:  new Sha256Hash("55555555").toHex(),
                fullName: 'Parampampam',
                email: 'pavel_bezpalov@geekhub.ck.ua',
                phone: '380000000000').save(failOnError: true)
        when: "The user is deleted"
        def foundUser = User.get(user.id)
        foundUser.delete(flush: true)
        then: "The user is removed from the database"
        !User.exists(foundUser.id)
    }

    def "Saving a user with invalid properties causes an error"() {
        given: "A user which fails several field validations"
        def pavel = new User(username: '',
                passwordHash:  new Sha256Hash("55555555").toHex(),
                fullName: 'Parampampam',
                email: '',
                phone: '380000000000')


        when: "The user is validated"
        pavel.validate()
        then:
        pavel.hasErrors()
        "nullable" == pavel.errors.getFieldError("username").code
        null == pavel.errors.getFieldError("username").rejectedValue
        "nullable" == pavel.errors.getFieldError("email").code
        null == pavel.errors.getFieldError("email").rejectedValue
        !pavel.errors.getFieldError("fullName")
    }

    def "Recovering from a failed save by fixing invalid properties"() {
        given: "A user that has invalid properties"
        def pavel = new User(username: 'Pavel',
                passwordHash:  new Sha256Hash("55555555").toHex(),
                fullName: 'PB',
                email: 'not-an-email',
                phone: '380000000000')
        assert pavel.save() == null
        assert pavel.hasErrors()
        when: "We fix the invalid properties"
        pavel.fullName = 'Pavel Bezpalov'
        pavel.email = 'pavel_bezpalov@geekhub.ck.ua'
        pavel.validate()
        then: "The user saves and validates fine"
        !pavel.hasErrors()
        pavel.save()
        User.get(pavel.id).fullName == 'Pavel Bezpalov'
    }

    def "User can have many courses and courses can have many users"() {
        given: "A user and a set of courses"
        def pavel = new User(username: 'pavel',
                passwordHash:  new Sha256Hash("55555555").toHex(),
                fullName: 'Pavel Bezpalov',
                email: 'pavel_bezpalov@geekhub.ck.ua',
                phone: '380000000000').save(failOnError: true)
        def olesya = new User(username: 'olesya',
                passwordHash:  new Sha256Hash("55555555").toHex(),
                fullName: 'Olesya',
                email: 'olesya@geekhub.ck.ua',
                phone: '380000000000').save(failOnError: true)
        def courseGroovy = new Course(name: "Groovy", description: 'Groovy', shortDescription: 'Groovy').save()
        def courseGrails = new Course(name: "Grails", description: 'Grails', shortDescription: 'Grails').save()
        when: "Add memberships to user"
        def membershipGroovyPavel = new Membership(course: courseGroovy, access: Access.SUBSCRIBED, user: pavel).save()
        def membershipGrailsPavel = new Membership(course: courseGrails, access: Access.TEACHER, user: pavel).save()
        def membershipGroovyOlesya = new Membership(course: courseGroovy, access: Access.SUBSCRIBED, user: olesya).save()
        def membershipGrailsOlesya = new Membership(course: courseGrails, access: Access.STUDENT, user: olesya).save()
        then:
        pavel.memberships*.course.name.sort() == ['Grails', 'Groovy']
        pavel.memberships.size() == 2
        courseGroovy.memberships.user.username == ['pavel', 'olesya']
        courseGrails.memberships.user.fullName == ['Pavel Bezpalov', 'Olesya']
    }

    def "Testing in memory operations"() {
        given: "A brand new user and two roles"
        def pavel = new User(username: 'pavel',
                passwordHash:  new Sha256Hash("55555555").toHex(),
                fullName: 'Pavel Bezpalov',
                email: 'pavel_bezpalov@geekhub.ck.ua',
                phone: '380000000000')
        def adminRole = new Role(name: "admin")
        def userRole = new Role(name: "user")
        when: "add both roles, remove adminRole and save user"
        pavel.addToRoles(adminRole)
        pavel.addToRoles(userRole)
        pavel.removeFromRoles(adminRole)
        pavel.save()
        then: "user saved successfully with only userRole"
        pavel.errors.errorCount == 0
        pavel.id != null
        User.get(pavel.id).fullName == pavel.fullName != null
        User.get(pavel.id).username == pavel.username
        User.findByUsername(pavel.username).roles.name == ["user"]
    }
}
